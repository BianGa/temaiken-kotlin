package com.utn.temaiken

import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class TemaikenApplicationTests {

    // To be done
//    @Test
//    fun contextLoads() {
//    }
}
