package com.utn.temaiken.integration.repository

import com.utn.temaiken.base.BaseIntegrationTest
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder
import org.hamcrest.core.IsEqual.equalTo
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class WeatherTypeRepositoryTest @Autowired constructor(
    val target: WeatherTypeRepository
) : BaseIntegrationTest() {

    @Test
    fun `When execute should return all loaded vegetation types`() {
        // When
        val result = target.findWeatherTypes()

        // Then
        val expectedWeatherTypes = listOf(
            "DRY_WEATHER",
            "WARM_WEATHER",
            "MILD_WEATHER",
            "POLAR_WEATHER",
            "TROPICAL_WEATHER",
            "MODERATE_WEATHER"
        )
        assertThat(result.size, equalTo(expectedWeatherTypes.size))
        assertThat(result.map { it.id }, containsInAnyOrder(*expectedWeatherTypes.toTypedArray()))
    }
}
