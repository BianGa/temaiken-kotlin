package com.utn.temaiken.integration.repository

import com.utn.temaiken.base.BaseIntegrationTest
import com.utn.temaiken.core.entity.model.Zone
import com.utn.temaiken.util.creator.ZoneCreator.createZone
import java.math.BigDecimal
import kotlin.random.Random
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder
import org.hamcrest.core.IsEqual.equalTo
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class ZoneRepositoryTest @Autowired constructor(
    val target: ZoneRepository
) : BaseIntegrationTest() {

    @Test
    fun `When execute findZones method should return all loaded zones`() {
        // Given
        val givenZones = mutableListOf<Zone>()

        (1L..3L).forEach { idx ->
            givenZones.add(
                Zone(
                    id = idx,
                    name = "ZONE_$idx",
                    extension = BigDecimal.valueOf(Random.nextDouble(until = Double.MAX_VALUE / 2)).setScale(2),
                    isActive = true
                )
            )
        }
        givenZones.map { zone ->
            createZone(zone)
        }

        // When
        val result = target.findZones()

        // Then
        val expectedZones = givenZones
        assertThat(result.size, equalTo(3))
        assertThat(result, containsInAnyOrder(*expectedZones.toTypedArray()))
    }

    @Test
    fun `when findByName is called, method should return the matching zone successfully`() {
        // Given
        val givenZoneName = "ZONE_4"
        val givenZones = mutableListOf<Zone>()

        (1L..3L).forEach { idx ->
            givenZones.add(
                Zone(
                    id = idx,
                    name = "ZONE_$idx",
                    extension = BigDecimal.valueOf(idx),
                    isActive = true
                )
            )
        }

        givenZones.add(
            Zone(
                id = 4L,
                name = givenZoneName,
                extension = BigDecimal.valueOf(4L),
                isActive = false
            )
        )

        givenZones.map { zone ->
            createZone(zone)
        }

        // When
        val result = target.findByName(givenZoneName)

        // Then
        val expectedZone = Zone(
            id = 4L,
            name = givenZoneName,
            extension = BigDecimal.valueOf(4L),
            isActive = false
        )
        assertThat(result, equalTo(expectedZone))
    }

    @Test
    fun `when findByIsActive is called, method should return the active zones exclusively`() {
        // Given
        val givenZones = mutableListOf<Zone>()

        (1L..3L).forEach { idx ->
            givenZones.add(
                Zone(
                    id = idx,
                    name = "ZONE_$idx",
                    extension = BigDecimal.valueOf(idx),
                    isActive = true
                )
            )
        }

        (4L..6L).forEach { idx ->
            givenZones.add(
                Zone(
                    id = idx,
                    name = "ZONE_$idx",
                    extension = BigDecimal.valueOf(idx),
                    isActive = false
                )
            )
        }

        givenZones.map { zone ->
            createZone(zone)
        }

        // When
        val results = target.findByIsActive()

        // Then
        val expectedIds = listOf(1L, 2L, 3L)
        assertThat(results.size, equalTo(3))
        assertThat(results.map { it.id }, containsInAnyOrder(*expectedIds.toTypedArray()))
    }
}
