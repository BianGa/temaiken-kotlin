package com.utn.temaiken.integration.repository

import com.utn.temaiken.base.BaseIntegrationTest
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder
import org.hamcrest.core.IsEqual.equalTo
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class ContinentRepositoryTest @Autowired constructor(
    val target: ContinentRepository
) : BaseIntegrationTest() {

    @Test
    fun `When execute should return all loaded continents`() {
        // When
        val result = target.findContinents()

        // Then
        val expectedContinentAmount = 8
        val expectedContinents = listOf(
            "ASIA",
            "NORTH_AMERICA",
            "CENTER_AMERICA",
            "SOUTH_AMERICA",
            "EUROPE",
            "AFRICA",
            "OCEANIA",
            "ANTARCTICA"
        )
        assertThat(result.size, equalTo(expectedContinentAmount))
        assertThat(result.map { it.id }, containsInAnyOrder(*expectedContinents.toTypedArray()))
    }
}
