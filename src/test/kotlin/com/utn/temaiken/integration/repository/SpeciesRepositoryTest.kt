package com.utn.temaiken.integration.repository

import com.utn.temaiken.base.BaseIntegrationTest
import com.utn.temaiken.core.entity.model.Habitat
import com.utn.temaiken.core.entity.model.Species
import com.utn.temaiken.core.entity.model.Zone
import com.utn.temaiken.util.creator.HabitatCreator.createHabitat
import com.utn.temaiken.util.creator.SpeciesCreator.createSpecies
import com.utn.temaiken.util.creator.ZoneCreator.createZone
import java.lang.Boolean.TRUE
import java.math.BigDecimal
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class SpeciesRepositoryTest @Autowired constructor(
    val target: SpeciesRepository
) : BaseIntegrationTest() {

    @Test
    fun `when findBySpanishName is called, method should return the matching species successfully`() {
        // Given
        val givenSpeciesName = "Especie 1"
        val givenSpecies = mutableListOf<Species>()
        val givenZone = Zone(
            id = 1L,
            name = "test-zone-1",
            extension = BigDecimal(11.1),
            isActive = TRUE
        )
        val givenHabitat = Habitat(
            id = 1L,
            name = "test-habitat-1",
            weatherTypeId = "DRY_WEATHER",
            vegetationTypeId = "FOREST",
            continentId = "ASIA",
            isActive = TRUE
        )
        createZone(givenZone)
        createHabitat(givenHabitat)

        (1L..3L).forEach { idx ->
            givenSpecies.add(
                Species(
                    id = idx,
                    spanishName = "Especie $idx",
                    scientificName = "test-species-$idx",
                    description = "description-species-$idx",
                    habitatId = givenHabitat.id,
                    zoneId = givenZone.id,
                    isActive = true
                )
            )
        }

        givenSpecies.map { species ->
            createSpecies(species)
        }

        // When
        val result = target.findBySpanishName(givenSpeciesName)

        // Then
        val expectedSpecies = Species(
            id = 1L,
            spanishName = "Especie 1",
            scientificName = "test-species-1",
            description = "description-species-1",
            habitatId = givenHabitat.id,
            zoneId = givenZone.id,
            isActive = true
        )
        assertThat(result, equalTo(expectedSpecies))
    }
}
