package com.utn.temaiken.integration.repository

import com.utn.temaiken.base.BaseIntegrationTest
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder
import org.hamcrest.core.IsEqual.equalTo
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class VegetationTypeRepositoryTest @Autowired constructor(
    val target: VegetationTypeRepository
) : BaseIntegrationTest() {

    @Test
    fun `When execute should return all loaded vegetation types`() {
        // When
        val result = target.findVegetationTypes()

        // Then
        val expectedVegetationNames = listOf(
            "FOREST",
            "JUNGLE",
            "TUNDRA",
            "STEEP",
            "DESSERT",
            "MONSON_FOREST",
            "FROZEN_DESSERT",
            "SAVANNAH"
        )
        assertThat(result.size, equalTo(expectedVegetationNames.size))
        assertThat(result.map { it.id }, containsInAnyOrder(*expectedVegetationNames.toTypedArray()))
    }
}
