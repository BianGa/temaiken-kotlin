package com.utn.temaiken.integration.repository

import com.utn.temaiken.base.BaseIntegrationTest
import com.utn.temaiken.core.constants.Roles
import com.utn.temaiken.core.entity.model.Payroll
import com.utn.temaiken.core.entity.model.Person
import com.utn.temaiken.core.entity.model.User
import com.utn.temaiken.util.creator.PayrollCreator.createPayroll
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import org.exparity.hamcrest.date.LocalDateTimeMatchers.within
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.beans.SamePropertyValuesAs.samePropertyValuesAs
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class PayrollRepositoryTest @Autowired constructor(
    val target: PayrollRepository
) : BaseIntegrationTest() {

    @Test
    fun `When execute findCompletePayroll method should return all loaded payroll list`() {
        // Given
        val givenUser = User(
            id = 1L,
            username = "pepito_username",
            password = "clavito",
            roleId = Roles.ADMIN.id,
            isActive = true
        )
        val givenPerson = Person(
            id = 1L,
            userId = givenUser.id,
            name = "pepito",
            lastname = "clavo un clavito",
            address = "calle falsa 123",
            phone = "0303 456",
            isActive = true
        )
        val givenPayroll = Payroll(
            id = 1L,
            personId = givenPerson.id,
            roleId = givenUser.roleId,
            hiringDate = LocalDateTime.now(),
            isActive = true
        )
        createPayroll(givenUser, givenPerson, givenPayroll)

        // When
        val result = target.findCompletePayroll()

        // Then
        val expectedPayroll = givenPayroll.copy()
        assertThat(result.first(), samePropertyValuesAs(expectedPayroll, "hiringDate"))
        assertThat(result.first().hiringDate, within(1, ChronoUnit.MINUTES, expectedPayroll.hiringDate))
    }
}
