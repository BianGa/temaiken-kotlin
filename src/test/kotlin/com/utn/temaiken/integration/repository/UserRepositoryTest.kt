package com.utn.temaiken.integration.repository

import com.utn.temaiken.base.BaseIntegrationTest
import com.utn.temaiken.core.constants.Roles
import com.utn.temaiken.core.entity.model.User
import com.utn.temaiken.util.creator.UserCreator.createUser
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.dao.EmptyResultDataAccessException

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class UserRepositoryTest @Autowired constructor(
    val target: UserRepository
) : BaseIntegrationTest() {

    @Test
    fun `When execute findUsers method should return all loaded users`() {
        // Given
        val givenUser = User(
            id = 0L,
            username = "ADMIN",
            password = "ADMIN",
            roleId = Roles.ADMIN.id,
            isActive = true
        )

        // When
        val result = target.findUsers()

        // Then
        val expectedUser = givenUser.copy()
        assertThat(result.size, equalTo(1))
        assertThat(result.first(), equalTo(expectedUser))
    }

    @Test
    fun `when searching user by correct username and password should return said user`() {
        // Given
        val givenUsername = "test-user"
        val givenPassword = "test-password"
        val givenUser = User(
            id = 10L,
            username = givenUsername,
            password = givenPassword,
            roleId = Roles.TOUR_GUIDE.id,
            isActive = true
        )
        createUser(givenUser)

        // When
        val result = target.findByUsernameAndPasswordAndIsActive(givenUsername, givenPassword)

        // Then
        val expectedUser = givenUser.copy()
        assertThat(result, equalTo(expectedUser))
    }

    @Test
    fun `when searching for a non-existent user should throw an exception`() {
        // Given
        val givenUsername = "test-user"
        val givenPassword = "test-password"

        // When
        var result = ""
        try {
            result = target.findByUsernameAndPasswordAndIsActive(givenUsername, givenPassword).toString()
        } catch (e: Exception) {
            if (e is EmptyResultDataAccessException) result = "EmptyResultDataAccessException"
        }

        // Then
        val expectedResult = "EmptyResultDataAccessException"
        assertThat(result, equalTo(expectedResult))
    }

    @Test
    fun `when searching for a certain user but its state is inactive should return an exception`() {
        // Given
        val givenInactiveUser = User(
            id = 1L,
            username = "inactive",
            password = "inactive",
            roleId = Roles.ZOOKEEPER.id,
            isActive = false
        )
        createUser(givenInactiveUser)

        // When
        var result = ""
        try {
            result = target.findByUsernameAndPasswordAndIsActive(
                givenInactiveUser.username,
                givenInactiveUser.password
            ).toString()
        } catch (e: Exception) {
            if (e is EmptyResultDataAccessException) result = "EmptyResultDataAccessException"
        }

        // Then
        val expectedResult = "EmptyResultDataAccessException"
        assertThat(result, equalTo(expectedResult))
    }
}
