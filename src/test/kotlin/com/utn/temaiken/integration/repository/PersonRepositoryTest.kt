package com.utn.temaiken.integration.repository

import com.utn.temaiken.base.BaseIntegrationTest
import com.utn.temaiken.core.constants.Roles
import com.utn.temaiken.core.entity.model.Person
import com.utn.temaiken.core.entity.model.User
import com.utn.temaiken.util.creator.PersonCreator.createPerson
import kotlin.random.Random
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class PersonRepositoryTest @Autowired constructor(
    val target: PersonRepository
) : BaseIntegrationTest() {

    @Test
    fun `When execute findPersons method should return all loaded persons`() {
        // Given
        val givenUser = User(
            id = 1L,
            username = "pepito_username",
            password = "clavito",
            roleId = Roles.ADMIN.id,
            isActive = true
        )
        val givenPerson = Person(
            id = 1L,
            userId = givenUser.id,
            name = "pepito",
            lastname = "clavo un clavito",
            address = "calle falsa 123",
            phone = "0303 456",
            isActive = true
        )
        createPerson(givenUser, givenPerson)

        // When
        val result = target.findPersons()

        // Then
        val expectedPerson = givenPerson.copy()
        assertThat(result.size, equalTo(2))
        assertThat(result.last(), equalTo(expectedPerson))
    }

    @Test
    fun `when findByUserId is called should return a person by its user id`() {
        // Given
        val givenUserId = 10L
        val givenUser = User(
            id = givenUserId,
            username = "pepito_username",
            password = "clavito",
            roleId = Roles.ADMIN.id,
            isActive = true
        )
        val givenPerson = Person(
            id = 1L,
            userId = givenUserId,
            name = "pepito",
            lastname = "clavo un clavito",
            address = "calle falsa 123",
            phone = "0303 456",
            isActive = true
        )
        createPerson(givenUser, givenPerson)

        // When
        val result = target.findByUserId(givenUserId)

        // Then
        val expectedResult = givenPerson.copy()
        assertThat(result, equalTo(expectedResult))
    }

    @Test
    fun `when findByNameAndLastnameAndIsActive is called should return a person by its name, lastname and active status`() {
        val givenUser = User(
            id = Random.nextLong(from = 0, until = Long.MAX_VALUE / 2),
            username = "pepito_username",
            password = "clavito",
            roleId = Roles.ZOOKEEPER.id,
            isActive = true
        )
        val givenPerson = Person(
            id = Random.nextLong(from = 0, until = Long.MAX_VALUE / 2),
            userId = givenUser.id,
            name = "pepito",
            lastname = "clavo un clavito",
            address = "calle falsa 123",
            phone = "0303 456",
            isActive = true
        )
        createPerson(givenUser, givenPerson)

        // When
        val result = target.findByNameAndLastnameAndIsActive(givenPerson.name, givenPerson.lastname)

        // Then
        val expectedPerson = givenPerson.copy()
        assertThat(result, equalTo(expectedPerson))
    }
}
