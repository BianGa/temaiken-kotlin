package com.utn.temaiken.integration.services

import com.utn.temaiken.base.BaseIntegrationTest
import com.utn.temaiken.core.constants.Roles
import com.utn.temaiken.core.entity.PayrollDeletionData
import com.utn.temaiken.core.entity.model.Person
import com.utn.temaiken.core.entity.model.User
import com.utn.temaiken.integration.services.payroll.PayrollDeletionService
import com.utn.temaiken.util.creator.PersonCreator.createPerson
import com.utn.temaiken.util.retriever.UsersRetriever.retrieveUsersById
import kotlin.random.Random
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class PayrollDeletionServiceIntegrationTest @Autowired constructor(
    private val target: PayrollDeletionService
) : BaseIntegrationTest() {

    @Test
    fun `when deletePayroll called with active payroll data should change isActive status to false`() {
        // Given
        val givenUser = User(
            id = Random.nextLong(from = 0, until = Long.MAX_VALUE / 2),
            username = "test-username",
            password = "test-password",
            roleId = Roles.ZOOKEEPER.id,
            isActive = true
        )
        val givenPerson = Person(
            id = Random.nextLong(from = 0, until = Long.MAX_VALUE / 2),
            userId = givenUser.id,
            name = "test-name",
            lastname = "test-lastname",
            address = "test-address",
            phone = "test-phone",
            isActive = true
        )
        createPerson(givenUser, givenPerson)

        val givenPayrollDeletionData = PayrollDeletionData(
            userId = givenUser.id,
            username = givenUser.username,
            name = givenPerson.name,
            lastname = givenPerson.lastname
        )

        // When
        target.deletePayroll(givenPayrollDeletionData)

        // Then
        val expectedIsActiveStatus = false
        val resultUsers = retrieveUsersById(givenUser.id)

        assertThat(resultUsers.size, equalTo(1))
        assertThat(resultUsers.first().isActive, equalTo(expectedIsActiveStatus))
        assertThat(resultUsers.first().username, equalTo(givenUser.username))
    }
}
