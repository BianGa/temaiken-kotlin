package com.utn.temaiken.integration.services

import com.utn.temaiken.base.BaseIntegrationTest
import com.utn.temaiken.core.entity.model.Zone
import com.utn.temaiken.util.creator.ZoneCreator.createZone
import com.utn.temaiken.util.retriever.ZonesRetriever.retrieveActiveZones
import java.math.BigDecimal
import kotlin.random.Random
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class ZoneServiceIntegrationTest @Autowired constructor(
    private val target: ZoneService
) : BaseIntegrationTest() {

    @Test
    fun `when given a valid zone name should change its activeness to false`() {
        // Given
        val givenZones = mutableListOf<Zone>()

        (1L..3L).forEach { idx ->
            givenZones.add(
                Zone(
                    id = idx,
                    name = "ZONE_$idx",
                    extension = BigDecimal.valueOf(
                        Random.nextDouble(until = Double.MAX_VALUE / 2)
                    ).setScale(2),
                    isActive = true
                )
            )
        }
        givenZones.map { zone ->
            createZone(zone)
        }
        // When
        val result = target.deleteZoneByName("ZONE_2")

        // Then
        val expectedActiveZones = 2
        assertThat(result, equalTo(1))
        assertThat(retrieveActiveZones().size, equalTo(expectedActiveZones))
    }
}
