package com.utn.temaiken.integration.services

import com.utn.temaiken.base.BaseIntegrationTest
import com.utn.temaiken.core.constants.Roles
import com.utn.temaiken.core.entity.model.Payroll
import com.utn.temaiken.core.entity.model.Person
import com.utn.temaiken.core.entity.model.User
import com.utn.temaiken.core.entity.payroll.ZookeeperCreationData
import com.utn.temaiken.integration.services.payroll.PayrollCreationService
import com.utn.temaiken.util.retriever.PayrollsRetriever.retrievePayrollsById
import com.utn.temaiken.util.retriever.PersonsRetriever.retrievePersonsById
import com.utn.temaiken.util.retriever.UsersRetriever.retrieveUsersById
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import org.exparity.hamcrest.date.LocalDateTimeMatchers.within
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.beans.SamePropertyValuesAs.samePropertyValuesAs
import org.hamcrest.core.IsEqual.equalTo
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class PayrollCreationServiceIntegrationTest @Autowired constructor(
    val target: PayrollCreationService
) : BaseIntegrationTest() {

    @Test
    fun `when createPayroll is called with valid information should create complete user`() {
        // Given
        val givenZookeeperCreationData = ZookeeperCreationData(
            username = "zookeeper1",
            password = "111",
            name = "test",
            lastname = "data",
            address = "any address",
            phone = "any phone number"
        )

        // When
        target.createPayroll(givenZookeeperCreationData)

        // Then
        val expectedUser = User(
            id = 1L,
            username = givenZookeeperCreationData.username,
            password = givenZookeeperCreationData.password,
            roleId = Roles.ZOOKEEPER.id,
            isActive = true
        )
        val expectedPerson = Person(
            id = 1L,
            userId = expectedUser.id,
            name = givenZookeeperCreationData.name,
            lastname = givenZookeeperCreationData.lastname,
            address = givenZookeeperCreationData.address,
            phone = givenZookeeperCreationData.phone,
            isActive = true
        )
        val expectedPayroll = Payroll(
            id = 1L,
            personId = expectedPerson.id,
            roleId = Roles.ZOOKEEPER.id,
            hiringDate = LocalDateTime.now(),
            isActive = true
        )

        val resultUsers = retrieveUsersById(expectedUser.id)
        val resultPersons = retrievePersonsById(expectedPerson.id)
        val resultPayrolls = retrievePayrollsById(expectedPayroll.id)

        assertThat(resultUsers.first(), equalTo(expectedUser))
        assertThat(resultPersons.first(), equalTo(expectedPerson))
        assertThat(resultPayrolls.first(), samePropertyValuesAs(expectedPayroll, "hiringDate"))
        assertThat(resultPayrolls.first().hiringDate, within(1, ChronoUnit.MINUTES, expectedPayroll.hiringDate))
    }
}
