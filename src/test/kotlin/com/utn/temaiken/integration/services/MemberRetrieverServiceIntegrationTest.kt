package com.utn.temaiken.integration.services

import com.utn.temaiken.base.BaseIntegrationTest
import com.utn.temaiken.core.constants.Roles
import com.utn.temaiken.core.entity.MemberData
import com.utn.temaiken.core.entity.model.Payroll
import com.utn.temaiken.core.entity.model.Person
import com.utn.temaiken.core.entity.model.User
import com.utn.temaiken.integration.services.payroll.MemberRetrieverService
import com.utn.temaiken.util.creator.PayrollCreator.createPayroll
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import kotlin.random.Random
import org.exparity.hamcrest.date.LocalDateTimeMatchers.within
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.beans.SamePropertyValuesAs.samePropertyValuesAs
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class MemberRetrieverServiceIntegrationTest @Autowired constructor(
    private val target: MemberRetrieverService
) : BaseIntegrationTest() {

    @Test
    fun `when called retrieveMemberByNameAndLastname should find a loaded member and retrieve it correctly`() {
        // Given
        val givenRole = Roles.TOUR_GUIDE
        val givenUser = User(
            id = Random.nextLong(from = 0, until = Long.MAX_VALUE / 2),
            username = "pepito_username",
            password = "clavito",
            roleId = givenRole.id,
            isActive = true
        )
        val givenPerson = Person(
            id = Random.nextLong(from = 0, until = Long.MAX_VALUE / 2),
            userId = givenUser.id,
            name = "pepito",
            lastname = "clavo un clavito",
            address = "calle falsa 123",
            phone = "0303 456",
            isActive = true
        )
        val givenPayroll = Payroll(
            id = Random.nextLong(from = 0, until = Long.MAX_VALUE / 2),
            personId = givenPerson.id,
            roleId = givenUser.roleId,
            hiringDate = LocalDateTime.now(),
            isActive = true
        )
        createPayroll(givenUser, givenPerson, givenPayroll)

        // When
        val result = target.retrieveMemberByNameAndLastname(givenPerson.name, givenPerson.lastname)

        // Then
        val expectedMemberData = MemberData(
            userId = givenUser.id,
            username = givenUser.username,
            name = givenPerson.name,
            lastname = givenPerson.lastname,
            role = givenRole,
            payrollId = givenPayroll.id,
            address = givenPerson.address,
            phone = givenPerson.phone,
            hiringDate = givenPayroll.hiringDate
        )
        assertThat(result, samePropertyValuesAs(expectedMemberData, "hiringDate"))
        assertThat(result.hiringDate, within(1, ChronoUnit.MINUTES, expectedMemberData.hiringDate))
    }
}
