package com.utn.temaiken.integration.services

import com.utn.temaiken.base.BaseIntegrationTest
import kotlin.random.Random
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class AssignmentServiceIntegrationTest @Autowired constructor(
    private val target: AssignmentService
) : BaseIntegrationTest() {

    @Test
    fun `if there are no species assigned should return an empty list`() {
        // Given
        val givenZookeeperId = Random.nextLong(from = 0, until = 1000)

        // When
        val result = target.retrieveAssignedSpecies(givenZookeeperId)

        // Then
        assertThat(result.size, equalTo(0))
    }
}
