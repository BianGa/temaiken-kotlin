package com.utn.temaiken.integration.services

import com.utn.temaiken.base.BaseIntegrationTest
import com.utn.temaiken.core.constants.Roles
import com.utn.temaiken.core.entity.LoggedUserData
import com.utn.temaiken.core.entity.model.Person
import com.utn.temaiken.core.entity.model.User
import com.utn.temaiken.util.creator.PersonCreator.createPerson
import kotlin.random.Random
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class UserLoginServiceIntegrationTest @Autowired constructor(
    private val target: UserLoginService
) : BaseIntegrationTest() {

    @Test
    fun `Get admin user from database`() {
        // Given
        val givenAdminUsername = "ADMIN"
        val givenAdminPassword = "ADMIN"

        // When
        val result = target.retrieveActiveUser(givenAdminUsername, givenAdminPassword)

        // Then
        val expectedAdminLoggedUserData = LoggedUserData(
            username = givenAdminUsername,
            password = givenAdminPassword,
            role = Roles.ADMIN,
            name = "ZEUS",
            lastname = "TODO PODEROSO",
            address = "TEMPLO MÁS ALTO DEL OLIMPO",
            phone = "0303 456"
        )
        assertThat(result, equalTo(expectedAdminLoggedUserData))
    }

    @Test
    fun `when function is called should retrieve an existing user from database regardless the activeness`() {
        // Given
        val givenUser = User(
            id = Random.nextLong(from = 0, until = Long.MAX_VALUE / 2),
            username = "test-username",
            password = "test-password",
            roleId = Roles.TOUR_GUIDE.id,
            isActive = false
        )
        val givenPerson = Person(
            id = Random.nextLong(from = 0, until = Long.MAX_VALUE / 2),
            userId = givenUser.id,
            name = "test-name",
            lastname = "test-lastname",
            address = "test-address",
            phone = "test-phone",
            isActive = false
        )
        createPerson(givenUser, givenPerson)

        // When
        val result = target.retrieveExistingUserByUsernameAndPassword(givenUser.username, givenUser.password)

        // Then
        val expectedUser = LoggedUserData(
            username = givenUser.username,
            password = givenUser.password,
            role = Roles.TOUR_GUIDE,
            name = givenPerson.name,
            lastname = givenPerson.lastname,
            address = givenPerson.address,
            phone = givenPerson.phone
        )
        assertThat(result, equalTo(expectedUser))
    }
}
