package com.utn.temaiken.integration.services

import com.utn.temaiken.base.BaseIntegrationTest
import com.utn.temaiken.core.entity.model.Habitat
import com.utn.temaiken.core.entity.model.Species
import com.utn.temaiken.core.entity.model.Zone
import com.utn.temaiken.integration.repository.SpeciesRepository
import com.utn.temaiken.util.creator.HabitatCreator.createHabitat
import com.utn.temaiken.util.creator.SpeciesCreator.createSpecies
import com.utn.temaiken.util.creator.ZoneCreator.createZone
import com.utn.temaiken.util.retriever.SpeciesRetriever.retrieveActiveSpecies
import java.lang.Boolean.TRUE
import java.math.BigDecimal
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class SpeciesServiceIntegrationTest @Autowired constructor(
    private val target: SpeciesService,
    private val speciesRepository: SpeciesRepository
) : BaseIntegrationTest() {

    @Test
    fun `when given a valid species spanish name should change its activeness to false`() {
        // Given
        val givenSpecies = mutableListOf<Species>()
        val givenSpeciesName = "Especie 2"
        val givenZone = Zone(
            id = 1L,
            name = "test-zone-1",
            extension = BigDecimal(11.1),
            isActive = TRUE
        )
        val givenHabitat = Habitat(
            id = 1L,
            name = "test-habitat-1",
            weatherTypeId = "DRY_WEATHER",
            vegetationTypeId = "FOREST",
            continentId = "ASIA",
            isActive = TRUE
        )
        createZone(givenZone)
        createHabitat(givenHabitat)

        (1L..3L).forEach { idx ->
            givenSpecies.add(
                Species(
                    id = idx,
                    spanishName = "Especie $idx",
                    scientificName = "test-species-$idx",
                    description = "description-species-$idx",
                    habitatId = givenHabitat.id,
                    zoneId = givenZone.id,
                    isActive = true
                )
            )
        }
        givenSpecies.map { species ->
            createSpecies(species)
        }
        // When
        val result = target.deleteSpeciesBySpanishName(givenSpeciesName)

        // Then
        val expectedActiveSpecies = 2
        assertThat(result, equalTo(1))
        assertThat(retrieveActiveSpecies().size, equalTo(expectedActiveSpecies))

        assertThat(speciesRepository.findBySpanishName(givenSpeciesName).isActive, equalTo(false))
    }
}
