package com.utn.temaiken.integration.services

import com.utn.temaiken.base.BaseIntegrationTest
import com.utn.temaiken.core.entity.itinerary.ItineraryCreationData
import com.utn.temaiken.core.entity.model.*
import com.utn.temaiken.core.util.twoDecimalScale
import com.utn.temaiken.util.creator.HabitatCreator.createHabitat
import com.utn.temaiken.util.creator.SpeciesCreator
import com.utn.temaiken.util.creator.ZoneCreator.createZone
import com.utn.temaiken.util.retriever.ItinerariesTimetableRetriever.retrieveAllCompleteItineraries
import com.utn.temaiken.util.retriever.SpeciesItineraryRetriever.retrieveAllSpeciesItineraries
import com.utn.temaiken.util.retriever.ZonesItineraryRetriever.retrieveAllZonesItineraries
import java.lang.Boolean
import java.math.BigDecimal
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import kotlin.Double
import kotlin.random.Random
import org.exparity.hamcrest.date.LocalDateTimeMatchers.within
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.beans.SamePropertyValuesAs.samePropertyValuesAs
import org.hamcrest.core.IsEqual.equalTo
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class ItineraryServiceIntegrationTest @Autowired constructor(
    private val target: ItineraryService
) : BaseIntegrationTest() {

    @Test
    fun `when given valid itinerary information should create it successfully`() {
        // Given
        val givenZones = mutableListOf<Zone>()
        val givenSpecies = mutableListOf<Species>()
        val givenHabitat = Habitat(
            id = 1L,
            name = "test-habitat-1",
            weatherTypeId = "DRY_WEATHER",
            vegetationTypeId = "FOREST",
            continentId = "ASIA",
            isActive = Boolean.TRUE
        )
        (1L..3L).forEach { idx ->
            givenZones.add(
                Zone(
                    id = idx,
                    name = "ZONE_$idx",
                    extension = BigDecimal.valueOf(
                        Random.nextDouble(until = Double.MAX_VALUE / 2)
                    ).setScale(2),
                    isActive = true
                )
            )
        }
        (1L..3L).forEach { idx ->
            givenSpecies.add(
                Species(
                    id = idx,
                    spanishName = "Especie $idx",
                    scientificName = "test-species-$idx",
                    description = "description-species-$idx",
                    habitatId = givenHabitat.id,
                    zoneId = givenZones[idx.toInt().minus(1)].id,
                    isActive = true
                )
            )
        }

        createHabitat(givenHabitat)
        givenZones.map { zone ->
            createZone(zone)
        }
        givenSpecies.map { species ->
            SpeciesCreator.createSpecies(species)
        }

        val givenItineraryCreationData = ItineraryCreationData(
            code = "TEST-CODE",
            duration = 100L,
            longitude = BigDecimal(27.2).twoDecimalScale(),
            maxVisitorsAmount = 50,
            startsAt = LocalDateTime.now(),
            endsAt = LocalDateTime.now().plusMinutes(100),
            species = listOf(1L, 2L, 3L),
            zones = listOf(1L, 2L, 3L)
        )

        // When
        val result = target.createItinerary(givenItineraryCreationData)

        // Then
        val expectedItinerary = Itinerary(
            id = 1L,
            code = "TEST-CODE",
            duration = 100L,
            longitude = BigDecimal(27.2).twoDecimalScale(),
            maxVisitorsAmount = 50,
        )
        val expectedItineraryTimeTable = ItineraryTimetable(
            id = 1L,
            itineraryId = expectedItinerary.id,
            startsAt = givenItineraryCreationData.startsAt,
            endsAt = givenItineraryCreationData.endsAt
        )

        val itinerariesDatabaseResults = retrieveAllCompleteItineraries()
        val zoneItineraryDatabaseResults = retrieveAllZonesItineraries()
        val speciesItineraryDatabaseResults = retrieveAllSpeciesItineraries()

        assertThat(itinerariesDatabaseResults.size, equalTo(1))
        assertThat(itinerariesDatabaseResults.keys.first(), equalTo(expectedItinerary))
        assertThat(
            itinerariesDatabaseResults.values.first(),
            samePropertyValuesAs(expectedItineraryTimeTable, "startsAt", "endsAt")
        )
        assertThat(
            itinerariesDatabaseResults.values.first().startsAt,
            within(1, ChronoUnit.MINUTES, expectedItineraryTimeTable.startsAt)
        )
        assertThat(
            itinerariesDatabaseResults.values.first().endsAt,
            within(1, ChronoUnit.MINUTES, expectedItineraryTimeTable.endsAt)
        )

        assertThat(result, equalTo(1))

        assertThat(zoneItineraryDatabaseResults.size, equalTo(3))
        assertThat(speciesItineraryDatabaseResults.size, equalTo(3))
    }

    @Test
    fun `at method call should retrieve all itineraries from now onwards`() {
        // Given
        val givenNow = LocalDateTime.now()
        val givenItineraries = mutableListOf<ItineraryCreationData>()
        (1L..10L).forEach { idx ->
            givenItineraries.add(
                ItineraryCreationData(
                    code = "TEST-CODE-$idx",
                    duration = BigDecimal(10L).toLong().times(idx),
                    longitude = BigDecimal(27.2).twoDecimalScale(),
                    maxVisitorsAmount = 50,
                    startsAt = LocalDateTime.now().minusHours(2).plusMinutes(idx.times(30)),
                    endsAt = LocalDateTime.now().plusMinutes(100),
                    species = listOf(),
                    zones = listOf()
                )
            )
        }

        givenItineraries.map { itineraryCreationData ->
            target.createItinerary(itineraryCreationData)
        }

        // When
        val result = target.retrieveEveryActiveItinerary(givenNow)

        // Then
        assertThat(result.size, equalTo(6))
        assertThat(result.first().code, equalTo("TEST-CODE-5"))
    }

    @Test
    fun `trying to create itineraries with incorrect zone ids should fail`() {
        // Given
        val givenItineraryCreationData = ItineraryCreationData(
            code = "TEST-CODE",
            duration = 100L,
            longitude = BigDecimal(27.2).twoDecimalScale(),
            maxVisitorsAmount = 50,
            startsAt = LocalDateTime.now(),
            endsAt = LocalDateTime.now().plusMinutes(100),
            species = listOf(1L, 2L, 3L),
            zones = listOf(1L, 2L, 3L)
        )

        // When
        val result = target.createItinerary(givenItineraryCreationData)

        // Then
        assertThat(result, equalTo(0L))
    }
}
