package com.utn.temaiken.base

import com.utn.temaiken.configurations.database.DataSourceConfig
import com.utn.temaiken.configurations.database.DatabaseClient
import com.utn.temaiken.util.SqlExecutor
import javax.sql.DataSource
import org.flywaydb.core.Flyway
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeAll

abstract class BaseIntegrationTest {

    companion object {
        lateinit var databaseClient: DatabaseClient
        lateinit var postgresDataSource: DataSource
        private val datasource = DataSourceConfig()
        private const val FLYWAY_LOCATION = "db/migration"

        @BeforeAll
        @JvmStatic
        fun deployApp() {
            databaseClient = DatabaseClient()
            postgresDataSource = datasource.getDataSource()
            runFlywayMigration(postgresDataSource)
        }

        private fun runFlywayMigration(dataSource: DataSource) {
            Flyway.configure()
                .dataSource(dataSource)
                .locations(FLYWAY_LOCATION)
                .load()
                .migrate()
        }
    }

    @AfterEach
    fun cleanApp() {
        // clearAllMocks() cuando implemente mockk-common
        SqlExecutor.executeFromQuery(
            deleterQuery(),
            postgresDataSource
        )
    }
    private fun deleterQuery(): String =
        """
           DELETE FROM species_itineraries;
           DELETE FROM zones_itineraries;
           DELETE FROM tour_guide_itineraries;
           DELETE FROM itineraries_timetable;
           DELETE FROM itineraries;
           DELETE FROM species;
           DELETE FROM habitats;
           DELETE FROM zones;
           DELETE FROM payrolls;
           DELETE FROM persons WHERE id NOT IN (0);
           DELETE FROM users WHERE id NOT IN (0);
            
           
           ALTER SEQUENCE species_itineraries_seq RESTART WITH 1;
           ALTER SEQUENCE zones_itineraries_seq RESTART WITH 1;
           ALTER SEQUENCE tour_guide_itineraries_seq RESTART WITH 1;
           ALTER SEQUENCE itineraries_timetable_seq RESTART WITH 1;
           ALTER SEQUENCE itineraries_seq RESTART WITH 1;
           ALTER SEQUENCE species_seq RESTART WITH 1;
           ALTER SEQUENCE habitats_seq RESTART WITH 1;
           ALTER SEQUENCE zones_seq RESTART WITH 1;
           ALTER SEQUENCE payrolls_seq RESTART WITH 1;
           ALTER SEQUENCE persons_seq RESTART WITH 1;
           ALTER SEQUENCE users_seq RESTART WITH 1;
        """.trimIndent()
}
