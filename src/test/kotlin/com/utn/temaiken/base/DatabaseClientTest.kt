package com.utn.temaiken.base

import com.utn.temaiken.configurations.database.DatabaseClient
import com.utn.temaiken.core.entity.model.User
import com.utn.temaiken.util.creator.UserCreator.createUser
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual.equalTo
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class DatabaseClientTest : BaseIntegrationTest() {

    private val target = DatabaseClient()

    @Test
    fun `Get all from database`() {
        // Given
        val givenUserId = 10L
        createUser(
            User(
                id = givenUserId,
                username = "pablito",
                password = "clavito",
                roleId = 1000L,
                isActive = true
            )
        )
        val givenQuery = "SELECT * FROM users"
        val givenParams = listOf<String>()

        // When
        val resultSet = target.executeQuery(givenQuery, givenParams)
        var resultId = 0L
        while (resultSet.next()) {
            resultId = resultSet.getLong("id")
        }

        // Then
        val expectedId = 10L
        assertThat(resultId, equalTo(expectedId))
    }

    @Test
    fun `Get an element with where statement`() {
        // Given
        val givenParams = listOf(10L)
        val givenQuery = "SELECT id FROM users WHERE id = $1"
        createUser(
            User(
                id = 10L,
                username = "pablito",
                password = "clavito",
                roleId = 1000L,
                isActive = true
            )
        )

        // When
        var resultId = 0L
        val resultSet = target.executeQuery(givenQuery, givenParams)
        while (resultSet.next()) {
            resultId = resultSet.getLong("id")
        }

        // Then
        val expectedId = givenParams.first()
        assertThat(resultId, equalTo(expectedId))
    }

    @Test
    fun `Insert element into database`() {
        // Given
        val givenParams = listOf(11L, "'some_username'", "'some_password'", 1000L, true)
        val givenQuery = """
                INSERT INTO users (id, username, password, role_id, is_active) 
                VALUES ($1, $2, $3, $4, $5) RETURNING id
        """.trimIndent()

        // When
        var resultId = 0L
        val resultSet = target.executeQuery(givenQuery, givenParams)
        while (resultSet.next()) {
            resultId = resultSet.getLong("id")
        }

        // Then
        val expectedId = givenParams.first()
        assertThat(resultId, equalTo(expectedId))
    }
}
