package com.utn.temaiken.util.retriever

import com.utn.temaiken.base.BaseIntegrationTest
import com.utn.temaiken.core.entity.model.Payroll
import java.time.LocalDateTime

object PayrollsRetriever : BaseIntegrationTest() {

    fun retrievePayrollsById(payrollId: Long): List<Payroll> {
        val resultSet = databaseClient.executeQuery(
            """
                SELECT * FROM PAYROLLS WHERE ID = $payrollId
            """.trimIndent()
        )
        val payrolls = mutableListOf<Payroll>()
        while (resultSet.next()) {
            payrolls.add(
                Payroll(
                    id = resultSet.getLong("id"),
                    personId = resultSet.getLong("person_id"),
                    roleId = resultSet.getLong("role_id"),
                    hiringDate = resultSet.getObject(4, LocalDateTime::class.java),
                    isActive = resultSet.getBoolean("is_active")
                )
            )
        }
        return payrolls
    }
}
