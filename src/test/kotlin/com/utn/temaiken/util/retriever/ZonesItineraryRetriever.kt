package com.utn.temaiken.util.retriever

import com.utn.temaiken.base.BaseIntegrationTest
import com.utn.temaiken.core.entity.model.ZonesItinerary

object ZonesItineraryRetriever : BaseIntegrationTest() {

    fun retrieveAllZonesItineraries(): List<ZonesItinerary> {
        val resultSet = databaseClient.executeQuery(
            """
                SELECT * FROM zones_itineraries;
            """.trimIndent()
        )
        val zonesItineraries = mutableListOf<ZonesItinerary>()
        while (resultSet.next()) {
            zonesItineraries.add(
                ZonesItinerary(
                    id = resultSet.getLong("id"),
                    zoneId = resultSet.getLong("zone_id"),
                    itineraryId = resultSet.getLong("itinerary_id"),
                )
            )
        }
        return zonesItineraries
    }
}
