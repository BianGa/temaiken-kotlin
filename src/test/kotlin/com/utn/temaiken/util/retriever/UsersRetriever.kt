package com.utn.temaiken.util.retriever

import com.utn.temaiken.base.BaseIntegrationTest
import com.utn.temaiken.core.entity.model.User

object UsersRetriever : BaseIntegrationTest() {

    fun retrieveUsersById(userId: Long): List<User> {
        val resultSet = databaseClient.executeQuery(
            """
                SELECT * FROM USERS WHERE ID = $userId
            """.trimIndent()
        )
        val users = mutableListOf<User>()
        while (resultSet.next()) {
            users.add(
                User(
                    id = resultSet.getLong("id"),
                    username = resultSet.getString("username"),
                    password = resultSet.getString("password"),
                    roleId = resultSet.getLong("role_id"),
                    isActive = resultSet.getBoolean("is_active")
                )
            )
        }
        return users
    }
}
