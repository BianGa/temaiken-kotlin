package com.utn.temaiken.util.retriever

import com.utn.temaiken.base.BaseIntegrationTest
import com.utn.temaiken.core.entity.model.Itinerary

object ItinerariesRetriever : BaseIntegrationTest() {

    fun retrieveAllItineraries(): List<Itinerary> {
        val resultSet = databaseClient.executeQuery(
            """
                SELECT * FROM itineraries;
            """.trimIndent()
        )
        val itineraries = mutableListOf<Itinerary>()
        while (resultSet.next()) {
            itineraries.add(
                Itinerary(
                    id = resultSet.getLong("id"),
                    code = resultSet.getString("code"),
                    duration = resultSet.getLong("duration"),
                    longitude = resultSet.getBigDecimal("longitude"),
                    maxVisitorsAmount = resultSet.getInt("max_visitors_amount")
                )
            )
        }
        return itineraries
    }
}
