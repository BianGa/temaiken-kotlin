package com.utn.temaiken.util.retriever

import com.utn.temaiken.base.BaseIntegrationTest
import com.utn.temaiken.core.entity.model.Person

object PersonsRetriever : BaseIntegrationTest() {

    fun retrievePersonsById(personId: Long): List<Person> {
        val resultSet = databaseClient.executeQuery(
            """
                SELECT * FROM PERSONS WHERE ID = $personId
            """.trimIndent()
        )
        val persons = mutableListOf<Person>()
        while (resultSet.next()) {
            persons.add(
                Person(
                    id = resultSet.getLong("id"),
                    userId = resultSet.getLong("user_id"),
                    name = resultSet.getString("name"),
                    lastname = resultSet.getString("lastname"),
                    address = resultSet.getString("address"),
                    phone = resultSet.getString("phone"),
                    isActive = resultSet.getBoolean("is_active")
                )
            )
        }
        return persons
    }
}
