package com.utn.temaiken.util.retriever

import com.utn.temaiken.base.BaseIntegrationTest
import com.utn.temaiken.core.entity.model.Species

object SpeciesRetriever : BaseIntegrationTest() {

    fun retrieveActiveSpecies(): List<Species> {
        val resultSet = databaseClient.executeQuery(
            """
                SELECT * FROM species WHERE is_active = TRUE;
            """.trimIndent()
        )
        val species = mutableListOf<Species>()
        while (resultSet.next()) {
            species.add(
                Species(
                    id = resultSet.getLong("id"),
                    spanishName = resultSet.getString("spanish_name"),
                    scientificName = resultSet.getString("scientific_name"),
                    description = resultSet.getString("description"),
                    habitatId = resultSet.getLong("habitat_id"),
                    zoneId = resultSet.getLong("zone_id"),
                    isActive = resultSet.getBoolean("is_active")
                )
            )
        }
        return species
    }
}
