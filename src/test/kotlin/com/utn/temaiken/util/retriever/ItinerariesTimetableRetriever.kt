package com.utn.temaiken.util.retriever

import com.utn.temaiken.base.BaseIntegrationTest
import com.utn.temaiken.core.entity.model.Itinerary
import com.utn.temaiken.core.entity.model.ItineraryTimetable
import com.utn.temaiken.util.retriever.ItinerariesRetriever.retrieveAllItineraries

object ItinerariesTimetableRetriever : BaseIntegrationTest() {

    fun retrieveAllCompleteItineraries(): Map<Itinerary, ItineraryTimetable> {
        val map = mutableMapOf<Itinerary, ItineraryTimetable>()

        val itineraries = retrieveAllItineraries()

        val resultSet = databaseClient.executeQuery(
            """
                SELECT * FROM itineraries_timetable;
            """.trimIndent()
        )
        val itinerariesTimetable = mutableListOf<ItineraryTimetable>()
        while (resultSet.next()) {
            itinerariesTimetable.add(
                ItineraryTimetable(
                    id = resultSet.getLong("id"),
                    itineraryId = resultSet.getLong("itinerary_id"),
                    startsAt = resultSet.getTimestamp("starts_at").toLocalDateTime(),
                    endsAt = resultSet.getTimestamp("ends_at").toLocalDateTime()
                )
            )
        }
        itineraries.map { itinerary ->
            itinerariesTimetable.find { it.itineraryId == itinerary.id }?.let { map.putIfAbsent(itinerary, it) }
        }
        return map
    }
}
