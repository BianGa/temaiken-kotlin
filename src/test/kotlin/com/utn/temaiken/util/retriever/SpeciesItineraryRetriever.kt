package com.utn.temaiken.util.retriever

import com.utn.temaiken.base.BaseIntegrationTest
import com.utn.temaiken.core.entity.model.SpeciesItinerary

object SpeciesItineraryRetriever : BaseIntegrationTest() {

    fun retrieveAllSpeciesItineraries(): List<SpeciesItinerary> {
        val resultSet = databaseClient.executeQuery(
            """
                SELECT * FROM species_itineraries;
            """.trimIndent()
        )
        val speciesItineraries = mutableListOf<SpeciesItinerary>()
        while (resultSet.next()) {
            speciesItineraries.add(
                SpeciesItinerary(
                    id = resultSet.getLong("id"),
                    speciesId = resultSet.getLong("species_id"),
                    itineraryId = resultSet.getLong("itinerary_id"),
                )
            )
        }
        return speciesItineraries
    }
}
