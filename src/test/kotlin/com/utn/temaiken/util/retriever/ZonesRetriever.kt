package com.utn.temaiken.util.retriever

import com.utn.temaiken.base.BaseIntegrationTest
import com.utn.temaiken.core.entity.model.Zone

object ZonesRetriever : BaseIntegrationTest() {

    fun retrieveActiveZones(): List<Zone> {
        val resultSet = databaseClient.executeQuery(
            """
                SELECT * FROM ZONES WHERE is_active = TRUE;
            """.trimIndent()
        )
        val zones = mutableListOf<Zone>()
        while (resultSet.next()) {
            zones.add(
                Zone(
                    id = resultSet.getLong("id"),
                    name = resultSet.getString("name"),
                    extension = resultSet.getBigDecimal("extension"),
                    isActive = resultSet.getBoolean("is_active")
                )
            )
        }
        return zones
    }
}
