package com.utn.temaiken.util.creator

import com.utn.temaiken.base.BaseIntegrationTest
import com.utn.temaiken.core.entity.model.User
import com.utn.temaiken.util.SqlExecutor

object UserCreator : BaseIntegrationTest() {

    fun createUser(user: User) {
        val query =
            """
                INSERT INTO users (id, username, password, role_id, is_active) 
                VALUES (%id%, 
                        '%username%', 
                        '%password%', 
                        %role_id%, 
                        %is_active%
                        );
            """.trimIndent()
        SqlExecutor.executeFromQuery(
            query,
            postgresDataSource,
            mapOf(
                "id" to user.id.toString(),
                "username" to user.username,
                "password" to user.password,
                "role_id" to user.roleId.toString(),
                "is_active" to user.isActive.toString()
            )
        )
    }
}
