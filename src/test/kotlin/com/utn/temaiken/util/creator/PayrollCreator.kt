package com.utn.temaiken.util.creator

import com.utn.temaiken.base.BaseIntegrationTest
import com.utn.temaiken.core.entity.model.Payroll
import com.utn.temaiken.core.entity.model.Person
import com.utn.temaiken.core.entity.model.User
import com.utn.temaiken.util.SqlExecutor
import com.utn.temaiken.util.creator.PersonCreator.createPerson

object PayrollCreator : BaseIntegrationTest() {

    fun createPayroll(
        user: User,
        person: Person,
        payroll: Payroll
    ) {
        createPerson(user, person)
        val query =
            """
                INSERT INTO payrolls (id, person_id, role_id, hiring_date, is_active)
                VALUES (
                        %id%, 
                        (select id from persons where name like '%person_name%'),
                        %role_id%,
                        '%hiring_date%',
                        %is_active%
                        )
            """.trimIndent()
        SqlExecutor.executeFromQuery(
            query,
            postgresDataSource,
            mapOf(
                "id" to payroll.id.toString(),
                "person_name" to person.name,
                "role_id" to user.roleId.toString(),
                "hiring_date" to payroll.hiringDate.toString(),
                "is_active" to payroll.isActive.toString()
            )
        )
    }
}
