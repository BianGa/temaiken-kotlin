package com.utn.temaiken.util.creator

import com.utn.temaiken.base.BaseIntegrationTest
import com.utn.temaiken.core.entity.model.Habitat
import com.utn.temaiken.util.SqlExecutor

object HabitatCreator : BaseIntegrationTest() {

    fun createHabitat(
        habitat: Habitat
    ) {
        val query =
            """
               INSERT INTO habitats (id, name, weather_type_id, vegetation_type_id, continent_id, is_active)
                VALUES (
                        %id%, 
                        '%name%',
                        '%weather_type_id%',
                        '%vegetation_type_id%',
                        '%continent_id%',
                        %is_active%
                        )
            """.trimIndent()
        SqlExecutor.executeFromQuery(
            query,
            postgresDataSource,
            mapOf(
                "id" to habitat.id.toString(),
                "name" to habitat.name,
                "weather_type_id" to habitat.weatherTypeId,
                "vegetation_type_id" to habitat.vegetationTypeId,
                "continent_id" to habitat.continentId,
                "is_active" to habitat.isActive.toString()
            )
        )
    }
}
