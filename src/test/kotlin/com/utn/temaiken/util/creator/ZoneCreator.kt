package com.utn.temaiken.util.creator

import com.utn.temaiken.base.BaseIntegrationTest
import com.utn.temaiken.core.entity.model.Zone
import com.utn.temaiken.util.SqlExecutor

object ZoneCreator : BaseIntegrationTest() {

    fun createZone(
        zone: Zone
    ) {
        val query =
            """
               INSERT INTO zones (id, name, extension, is_active)
                VALUES (
                        %id%, 
                        '%name%',
                        %extension%, 
                        %is_active%
                        )
            """.trimIndent()
        SqlExecutor.executeFromQuery(
            query,
            postgresDataSource,
            mapOf(
                "id" to zone.id.toString(),
                "name" to zone.name,
                "extension" to zone.extension.toString(),
                "is_active" to zone.isActive.toString()
            )
        )
    }
}
