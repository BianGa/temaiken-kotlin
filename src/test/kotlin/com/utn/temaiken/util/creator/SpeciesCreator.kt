package com.utn.temaiken.util.creator

import com.utn.temaiken.base.BaseIntegrationTest
import com.utn.temaiken.core.entity.model.Species
import com.utn.temaiken.util.SqlExecutor

object SpeciesCreator : BaseIntegrationTest() {

    fun createSpecies(
        species: Species
    ) {
        val query =
            """
               INSERT INTO species (id, spanish_name, scientific_name, description, habitat_id, zone_id, is_active)
                VALUES (
                        %id%, 
                        '%spanish_name%',
                        '%scientific_name%',
                        '%description%',
                        %habitat_id%, 
                        %zone_id%, 
                        %is_active%
                        )
            """.trimIndent()
        SqlExecutor.executeFromQuery(
            query,
            postgresDataSource,
            mapOf(
                "id" to species.id.toString(),
                "spanish_name" to species.spanishName,
                "scientific_name" to species.scientificName,
                "description" to species.description,
                "habitat_id" to species.habitatId.toString(),
                "zone_id" to species.zoneId.toString(),
                "is_active" to species.isActive.toString()
            )
        )
    }
}
