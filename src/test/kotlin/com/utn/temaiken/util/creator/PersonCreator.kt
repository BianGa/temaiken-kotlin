package com.utn.temaiken.util.creator

import com.utn.temaiken.base.BaseIntegrationTest
import com.utn.temaiken.core.entity.model.Person
import com.utn.temaiken.core.entity.model.User
import com.utn.temaiken.util.SqlExecutor
import com.utn.temaiken.util.creator.UserCreator.createUser

object PersonCreator : BaseIntegrationTest() {

    fun createPerson(user: User, person: Person) {
        createUser(user)
        val query =
            """
                INSERT INTO persons (id, user_id, name, lastname, address, phone, is_active)
                VALUES (
                        %id%, 
                        (select id from users where username like '%username%'),
                        '%name%',
                        '%lastname%',
                        '%address%',
                        '%phone%',
                        %is_active%
                        );
            """.trimIndent()
        SqlExecutor.executeFromQuery(
            query,
            postgresDataSource,
            mapOf(
                "id" to person.id.toString(),
                "username" to user.username,
                "name" to person.name,
                "lastname" to person.lastname,
                "address" to person.address,
                "phone" to person.phone,
                "is_active" to person.isActive.toString()
            )
        )
    }
}
