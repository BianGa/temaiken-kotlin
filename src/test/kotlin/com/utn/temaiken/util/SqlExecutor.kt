package com.utn.temaiken.util

import javax.sql.DataSource
import org.springframework.core.io.ClassPathResource

object SqlExecutor {

    fun executeFromFile(
        location: String,
        dataSource: DataSource,
        params: Map<String, String> = mapOf()
    ) {
        var query = ClassPathResource(
            location,
            this.javaClass.classLoader
        ).inputStream.reader().readText().trimIndent()
//       var query =   javaClass.classLoader.getResourceAsStream(location).reader().readText().trimIndent()

        params.forEach { query = query.replace("%${it.key}%", it.value) }
        dataSource.connection.use { connection ->
            connection.autoCommit = true
            connection.createStatement().use { statement ->
                statement.execute(query)
            }
        }
    }

    fun executeFromQuery(
        query: String,
        dataSource: DataSource,
        params: Map<String, String> = mapOf()
    ) {
        var preparedQuery = query
        if (params.isNotEmpty()) params.forEach {
            preparedQuery = preparedQuery.replace("%${it.key}%", it.value)
        } else {
            preparedQuery = query
        }
        dataSource.connection.use { connection ->
            connection.autoCommit = true
            connection.createStatement().use { statement ->
                statement.execute(preparedQuery)
            }
        }
    }
}
