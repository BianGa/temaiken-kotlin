insert into users (id, username, password, role_id, is_active)
values (1, 'pedro.picapiedra', 'yabbadabbadoo', 2000, true),
       (2, 'vilma.picapiedra', 'perlas', 2000, true),
       (3, 'pablo.marmol', 'mejor_amigo', 3000, true),
       (4, 'betty.marmol', 'collar', 3000, true),
       (5, 'pebbles.picapiedra', 'huesito', 4000, true),
       (6, 'bam.bam.marmol', 'fuerza', 4000, true);

insert into persons (id, user_id, name, lastname, address, phone, is_active)
values  (1, 1, 'Pedro', 'Picapiedra', 'Piedradura 1', '0000', true),
        (2, 2, 'Vilma', 'Picapiedra', 'Piedradura 1', '0000', true),
        (3, 3, 'Pablo', 'Mármol', 'Piedradura 2', '0000', true),
        (4, 4, 'Betty', 'Mármol', 'Piedradura 2', '0000', true),
        (5, 5, 'Pebbles', 'Picapidra', 'Piedradura 1', '0000', true),
        (6, 6, 'Bam Bam', 'Mármol', 'Piedradura 2', '0000', true);

insert into payrolls (id, person_id, role_id, hiring_date, is_active)
values (1, 1, 2000, now(), true),
       (2, 2, 2000, now(), true),
       (3, 3, 3000, now(), true),
       (4, 4, 3000, now(), true);

insert into zones (id, name, extension, is_active)
values (1, 'zone_1', 33, true),
       (2, 'zone_2', 9.5, true),
       (3, 'zone_3', 4.2, true),
       (4, 'zone_4', 16, true),
       (5, 'zone_5', 56, true);

insert into habitats (id, name, weather_type_id, vegetation_type_id, continent_id, is_active)
values (1, 'habitat_1', 'DRY_WEATHER', 'FOREST', 'CENTER_AMERICA', true),
       (2, 'habitat_2', 'WARM_WEATHER', 'TUNDRA', 'SOUTH_AMERICA', true),
       (3, 'habitat_3', 'MILD_WEATHER', 'STEEP', 'EUROPE', true),
       (4, 'habitat_4', 'POLAR_WEATHER', 'JUNGLE', 'AFRICA', true),
       (5, 'habitat_5', 'TROPICAL_WEATHER', 'DESSERT', 'OCEANIA', true);

insert into species (id, spanish_name, scientific_name, description, habitat_id, zone_id, is_active)
values  (1, 'especie_1', 'nombre_científico_1', 'descripción_1', 2, 1, true),
        (2, 'especie_2', 'nombre_científico_2', 'descripción_2', 3, 2, true),
        (3, 'especie_3', 'nombre_científico_3', 'descripción_3', 4, 3, true),
        (4, 'especie_4', 'nombre_científico_4', 'descripción_4', 5, 4, true),
        (5, 'especie_5', 'nombre_científico_5', 'descripción_5', 1, 5, true);