--------------------- SPECIES_ZOOKEEPER ---------------------
CREATE SEQUENCE IF NOT EXISTS species_zookeeper_seq;

CREATE TABLE IF NOT EXISTS species_zookeeper
(
    id              BIGINT DEFAULT nextval('species_zookeeper_seq')     NOT NULL,
    zookeeper_id    BIGINT                                              NOT NULL,
    species_id      BIGINT                                              NOT NULL,
    starting_date   TIMESTAMP                                           NOT NULL,

    CONSTRAINT species_zookeeper_pk_id PRIMARY KEY (id),
    CONSTRAINT species_zookeeper_uk_assignment UNIQUE (zookeeper_id, species_id),
    CONSTRAINT species_zookeeper_fk_zookeeper FOREIGN KEY (zookeeper_id) REFERENCES payrolls(id),
    CONSTRAINT species_zookeeper_fk_species_id FOREIGN KEY (species_id) REFERENCES species(id)
);