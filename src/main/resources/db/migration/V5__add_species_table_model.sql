--------------------- SPECIES ---------------------
CREATE SEQUENCE IF NOT EXISTS species_seq;

CREATE TABLE IF NOT EXISTS species
(
    id                  BIGINT DEFAULT nextval('species_seq')       NOT NULL,
    spanish_name        VARCHAR(100)                                NOT NULL,
    scientific_name     VARCHAR                                     NOT NULL,
    description         VARCHAR                                     NOT NULL,
    habitat_id          BIGINT                                      NOT NULL,
    zone_id             BIGINT                                      NOT NULL,
    is_active           BOOLEAN                                     NOT NULL,

    CONSTRAINT species_pk_id PRIMARY KEY (id),
    CONSTRAINT species_uk_spanish_name UNIQUE (spanish_name),
    CONSTRAINT species_uk_scientific_name UNIQUE (scientific_name),
    CONSTRAINT species_fk_habitat_id FOREIGN KEY (habitat_id) REFERENCES habitats(id),
    CONSTRAINT species_fk_zone_id FOREIGN KEY (zone_id) REFERENCES zones(id)
);