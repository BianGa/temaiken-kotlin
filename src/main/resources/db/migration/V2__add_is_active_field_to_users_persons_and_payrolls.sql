--------------------- USERS ---------------------
ALTER TABLE users ADD COLUMN is_active BOOLEAN;

UPDATE users SET is_active = TRUE;

ALTER TABLE users ALTER COLUMN is_active SET NOT NULL;

--------------------- PERSONS ---------------------
ALTER TABLE persons ADD COLUMN is_active BOOLEAN;

UPDATE persons SET is_active = TRUE;

ALTER TABLE persons ALTER COLUMN is_active SET NOT NULL;

--------------------- PAYROLLS ---------------------
ALTER TABLE payrolls ADD COLUMN is_active BOOLEAN;

UPDATE payrolls SET is_active = TRUE;

ALTER TABLE payrolls ALTER COLUMN is_active SET NOT NULL;