--------------------- WEATHER_TYPE ---------------------
CREATE TABLE IF NOT EXISTS weather_type
(
    id      VARCHAR(100)    NOT NULL,
    name    VARCHAR(100)    NOT NULL,

    CONSTRAINT weather_type_pk_id PRIMARY KEY (id),
    CONSTRAINT weather_type_uk_name UNIQUE (name)
);

--------------------- VEGETATION_TYPE ---------------------
CREATE TABLE IF NOT EXISTS vegetation_type
(
    id      VARCHAR(100)    NOT NULL,
    name    VARCHAR(100)    NOT NULL,

    CONSTRAINT vegetation_type_pk_id PRIMARY KEY (id),
    CONSTRAINT vegetation_type_uk_name UNIQUE (name)
);

--------------------- CONTINENTS ---------------------
CREATE TABLE IF NOT EXISTS continents
(
    id      VARCHAR(100)    NOT NULL,
    name    VARCHAR(100)    NOT NULL,

    CONSTRAINT continents_pk_id PRIMARY KEY (id),
    CONSTRAINT continents_uk_name UNIQUE (name)
);

--------------------- HABITATS ---------------------
CREATE SEQUENCE IF NOT EXISTS habitats_seq;

CREATE TABLE IF NOT EXISTS habitats
(
    id                  BIGINT DEFAULT nextval('habitats_seq')      NOT NULL,
    name                VARCHAR(100)                                NOT NULL,
    weather_type_id     VARCHAR                                     NOT NULL,
    vegetation_type_id  VARCHAR                                     NOT NULL,
    continent_id        VARCHAR                                     NOT NULL,
    is_active           BOOLEAN                                     NOT NULL,

    CONSTRAINT habitats_pk_id PRIMARY KEY (id),
    CONSTRAINT habitats_uk_name UNIQUE (name),
    CONSTRAINT habitats_fk_weather_type FOREIGN KEY (weather_type_id) REFERENCES weather_type(id),
    CONSTRAINT habitats_fk_vegetation_type FOREIGN KEY (vegetation_type_id) REFERENCES vegetation_type(id),
    CONSTRAINT habitats_fk_continent_id FOREIGN KEY (continent_id) REFERENCES continents(id)
);