--------------------- TOUR_GUIDE_ITINERARIES ---------------------
CREATE SEQUENCE IF NOT EXISTS tour_guide_itineraries_seq;

CREATE TABLE IF NOT EXISTS tour_guide_itineraries
(
    id                      BIGINT DEFAULT nextval('tour_guide_itineraries_seq')    NOT NULL,
    tour_guide_id           BIGINT                                                  NOT NULL,
    itinerary_timetable_id  BIGINT                                                  NOT NULL,

    CONSTRAINT tour_guide_itineraries_pk_id PRIMARY KEY (id),
    CONSTRAINT tour_guide_itineraries_uk_assignment UNIQUE (tour_guide_id, itinerary_timetable_id),
    CONSTRAINT tour_guide_itineraries_fk_guide FOREIGN KEY (tour_guide_id) REFERENCES payrolls(id),
    CONSTRAINT tour_guide_itineraries_fk_timetable FOREIGN KEY (itinerary_timetable_id) REFERENCES itineraries_timetable(id)
);
