--------------------- ITINERARIES ---------------------
CREATE SEQUENCE IF NOT EXISTS itineraries_seq;

CREATE TABLE IF NOT EXISTS itineraries
(
    id                      BIGINT DEFAULT nextval('itineraries_seq')   NOT NULL,
    code                    VARCHAR                                     NOT NULL,
    duration                INTEGER                                     NOT NULL,
    longitude               NUMERIC                                      NOT NULL,
    max_visitors_amount     INTEGER                                     NOT NULL,

    CONSTRAINT itineraries_pk_id PRIMARY KEY (id),
    CONSTRAINT itineraries_uk_code UNIQUE (code)
);

--------------------- ITINERARIES_TIMETABLE ---------------------
CREATE SEQUENCE IF NOT EXISTS itineraries_timetable_seq;

CREATE TABLE IF NOT EXISTS itineraries_timetable
(
    id              BIGINT DEFAULT nextval('itineraries_timetable_seq')   NOT NULL,
    itinerary_id    BIGINT                                                NOT NULL,
    starts_at       TIMESTAMP                                             NOT NULL,
    ends_at         TIMESTAMP                                             NOT NULL,

    CONSTRAINT itineraries_timetable_pk_id PRIMARY KEY (id),
    CONSTRAINT itineraries_timetable_fk_itinerary_id FOREIGN KEY (itinerary_id) REFERENCES itineraries(id),
    CONSTRAINT itineraries_timetable_uk_duration UNIQUE (starts_at, ends_at)
);