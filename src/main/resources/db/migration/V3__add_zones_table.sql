--------------------- ZONES ---------------------
CREATE SEQUENCE IF NOT EXISTS zones_seq;

CREATE TABLE IF NOT EXISTS zones
(
    id          BIGINT DEFAULT nextval('zones_seq')     NOT NULL,
    name        VARCHAR(100)                            NOT NULL,
    extension   NUMERIC                                 NOT NULL,
    is_active   BOOLEAN                                 NOT NULL,

    CONSTRAINT zones_pk_id PRIMARY KEY (id),
    CONSTRAINT zones_uk_name UNIQUE (name)
);