--------------------- WEATHER_TYPE ---------------------
INSERT INTO weather_type (id, name)
VALUES ('DRY_WEATHER', 'CLIMA SECO'),
       ('WARM_WEATHER', 'CLIMA CÁLIDO'),
       ('MILD_WEATHER', 'CLIMA TEMPLADO'),
       ('POLAR_WEATHER', 'CLIMA POLAR'),
       ('TROPICAL_WEATHER', 'CLIMA TROPICAL'),
       ('MODERATE_WEATHER', 'CLIMA MODERADO');

--------------------- VEGETATION_TYPE ---------------------
INSERT INTO vegetation_type (id, name)
VALUES ('FOREST', 'BOSQUE'),
       ('JUNGLE', 'SELVA'),
       ('TUNDRA', 'TUNDRA'),
       ('STEEP', 'ESTEPA'),
       ('DESSERT', 'DESIERTO'),
       ('MONSON_FOREST', 'BOSQUE MONZÓNICO'),
       ('FROZEN_DESSERT', 'DESIERTO HELADO'),
       ('SAVANNAH', 'SABANA');

--------------------- CONTINENTS ---------------------
INSERT INTO continents (id, name)
VALUES ('ASIA', 'ASIA'),
       ('NORTH_AMERICA', 'AMÉRICA DEL NORTE'),
       ('CENTER_AMERICA', 'AMÉRICA CENTRAL'),
       ('SOUTH_AMERICA', 'AMÉRICA DEL SUR'),
       ('EUROPE', 'EUROPA'),
       ('AFRICA', 'ÁFRICA'),
       ('OCEANIA', 'OCEANIA'),
       ('ANTARCTICA', 'ANTÁRTIDA');