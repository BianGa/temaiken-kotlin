--------------------- SPECIES_ITINERARIES ---------------------
CREATE SEQUENCE IF NOT EXISTS species_itineraries_seq;

CREATE TABLE IF NOT EXISTS species_itineraries
(
    id              BIGINT DEFAULT nextval('species_itineraries_seq')   NOT NULL,
    itinerary_id    BIGINT                                              NOT NULL,
    species_id      BIGINT                                              NOT NULL,

    CONSTRAINT species_itineraries_pk_id PRIMARY KEY (id),
    CONSTRAINT species_itineraries_uk_composite UNIQUE (itinerary_id, species_id),
    CONSTRAINT species_itineraries_fk_itinerary FOREIGN KEY (itinerary_id) REFERENCES itineraries(id),
    CONSTRAINT species_itineraries_fk_species FOREIGN KEY (species_id) REFERENCES species(id)
);

--------------------- ZONES_ITINERARIES ---------------------
CREATE SEQUENCE IF NOT EXISTS zones_itineraries_seq;

CREATE TABLE IF NOT EXISTS zones_itineraries
(
    id              BIGINT DEFAULT nextval('zones_itineraries_seq')   NOT NULL,
    itinerary_id    BIGINT                                            NOT NULL,
    zone_id         BIGINT                                            NOT NULL,

    CONSTRAINT zones_itineraries_pk_id PRIMARY KEY (id),
    CONSTRAINT zones_itineraries_uk_composite UNIQUE (itinerary_id, zone_id),
    CONSTRAINT zones_itineraries_fk_itinerary FOREIGN KEY (itinerary_id) REFERENCES itineraries(id),
    CONSTRAINT zones_itineraries_fk_zones FOREIGN KEY (zone_id) REFERENCES species(id)
);
