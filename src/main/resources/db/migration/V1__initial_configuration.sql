--------------------- ROLES ---------------------
CREATE TABLE IF NOT EXISTS roles
(
    id              BIGINT          NOT NULL,
    name            VARCHAR         NOT NULL,
    description     VARCHAR         NOT NULL,

    CONSTRAINT roles_pk_id PRIMARY KEY (id),
    CONSTRAINT roles_uk_name UNIQUE (name),
    CONSTRAINT roles_uk_description UNIQUE (description)
);

INSERT INTO roles (id, name, description)
VALUES (1000, 'ADMIN', 'ADMINISTRADOR'),
       (2000, 'ZOOKEEPER', 'CUIDADOR'),
       (3000, 'TOUR_GUIDE', 'GUIA'),
       (4000, 'VISITOR', 'VISITANTE')
    ON CONFLICT DO NOTHING;

--------------------- USERS ---------------------
CREATE SEQUENCE IF NOT EXISTS users_seq;

CREATE TABLE IF NOT EXISTS users
(
    id          BIGINT DEFAULT nextval('users_seq')     NOT NULL,
    username    VARCHAR (100)                           NOT NULL,
    password    VARCHAR (100)                           NOT NULL,
    role_id     BIGINT                                  NOT NULL,

    CONSTRAINT users_pk_id PRIMARY KEY (id),
    CONSTRAINT users_uk_username UNIQUE (username),
    CONSTRAINT users_fk_role_id FOREIGN KEY (role_id) REFERENCES roles (id)
);

INSERT INTO users (id, username, password, role_id)
VALUES (0, 'ADMIN', 'ADMIN', 1000)
    ON CONFLICT DO NOTHING;

--------------------- PERSONS ---------------------
CREATE SEQUENCE IF NOT EXISTS persons_seq;

CREATE TABLE IF NOT EXISTS persons
(
    id          BIGINT DEFAULT nextval('persons_seq')   NOT NULL,
    user_id     BIGINT                                  NOT NULL,
    name        VARCHAR (100)                           NOT NULL,
    lastname    VARCHAR (100)                           NOT NULL,
    address     VARCHAR (100)                           NOT NULL,
    phone       VARCHAR (100)                           NOT NULL,

    CONSTRAINT persons_pk_id PRIMARY KEY (id),
    CONSTRAINT persons_uk_user_id UNIQUE (user_id),
    CONSTRAINT persons_fk_user_id FOREIGN KEY (user_id) REFERENCES users (id)
);

INSERT INTO persons (id, user_id, name, lastname, address, phone)
VALUES (0, 0, 'ZEUS', 'TODO PODEROSO', 'TEMPLO MÁS ALTO DEL OLIMPO', '0303 456')
    ON CONFLICT DO NOTHING;

--------------------- PAYROLLS ---------------------
CREATE SEQUENCE IF NOT EXISTS payrolls_seq;

CREATE TABLE IF NOT EXISTS payrolls
(
    id          BIGINT DEFAULT nextval('payrolls_seq')  NOT NULL,
    person_id   BIGINT                                  NOT NULL,
    role_id     BIGINT                                  NOT NULL,
    hiring_date TIMESTAMP                               NOT NULL,

    CONSTRAINT payrolls_pk_id PRIMARY KEY (id),
    CONSTRAINT payrolls_uk_person_id UNIQUE (person_id),
    CONSTRAINT payrolls_fk_person_id FOREIGN KEY (person_id) REFERENCES persons (id),
    CONSTRAINT payrolls_fk_role_id FOREIGN KEY (role_id) REFERENCES roles (id)
);