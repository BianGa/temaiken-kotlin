package com.utn.temaiken.core.constants

enum class Roles(
    val id: Long,
    val description: String
) {

    ERROR(0L, "ERRROR"),
    ADMIN(1000L, "ADMINISTRADOR"),
    ZOOKEEPER(2000L, "CUIDADOR"),
    TOUR_GUIDE(3000L, "GUÍA"),
    VISITOR(4000L, "VISITANTE");

    companion object {
        fun getById(id: Long): Roles =
            values().find { it.id == id }
                ?: throw Exception("No se encontraron roles con el id: $id")

        fun getByName(name: String): Roles =
            values().find { it.name == name }
                ?: throw Exception("No se encontraron roles con el name: $name")
    }
}
