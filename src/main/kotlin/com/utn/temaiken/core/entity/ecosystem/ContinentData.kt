package com.utn.temaiken.core.entity.ecosystem

data class ContinentData(
    val id: String,
    val name: String,
)
