package com.utn.temaiken.core.entity.ecosystem.creation

import com.utn.temaiken.core.entity.ecosystem.VegetationTypeData
import com.utn.temaiken.core.entity.ecosystem.WeatherTypeData
import com.utn.temaiken.core.entity.model.Continent
import java.lang.Boolean.TRUE

data class HabitatCreationData(
    val name: String,
    val weather: WeatherTypeData,
    val vegetation: VegetationTypeData,
    val continent: Continent,
    val isActive: Boolean? = TRUE
)
