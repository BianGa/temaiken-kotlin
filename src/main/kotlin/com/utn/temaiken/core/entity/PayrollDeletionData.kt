package com.utn.temaiken.core.entity

data class PayrollDeletionData(
    val userId: Long,
    val username: String,
    val name: String,
    val lastname: String
)
