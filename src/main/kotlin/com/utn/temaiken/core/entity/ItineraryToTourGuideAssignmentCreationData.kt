package com.utn.temaiken.core.entity

data class ItineraryToTourGuideAssignmentCreationData(
    override val payrollId: Long, // touGuideId
    override val assignmentId: Long, // itineraryTimetableId
) : AssigningCreationData
