package com.utn.temaiken.core.entity.ecosystem

data class ZoneData(
    val id: Long,
    val name: String,
)
