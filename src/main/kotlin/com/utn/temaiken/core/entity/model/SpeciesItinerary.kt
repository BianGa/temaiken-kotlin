package com.utn.temaiken.core.entity.model

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table

@Table("species_itineraries")
data class SpeciesItinerary(
    @Id val id: Long,
    val speciesId: Long,
    val itineraryId: Long
)
