package com.utn.temaiken.core.entity

import com.utn.temaiken.core.constants.Roles

data class LoggedUserData(
    val username: String,
    val password: String,
    val role: Roles,
    val name: String,
    val lastname: String,
    val address: String,
    val phone: String
)
