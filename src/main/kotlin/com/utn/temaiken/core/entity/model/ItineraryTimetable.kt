package com.utn.temaiken.core.entity.model

import java.time.LocalDateTime
import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table

@Table("itineraries_timetable")
data class ItineraryTimetable(
    @Id val id: Long,
    val itineraryId: Long,
    val startsAt: LocalDateTime,
    val endsAt: LocalDateTime
)
