package com.utn.temaiken.core.entity.payroll

import com.utn.temaiken.core.constants.Roles
import java.time.LocalDateTime

interface PayrollCreationData {
    val username: String
    val password: String
    val name: String
    val lastname: String
    val address: String
    val phone: String
    val hiringDate: LocalDateTime
    val role: Roles
    val isActive: Boolean
}
