package com.utn.temaiken.core.entity

interface AssigningCreationData {
    val payrollId: Long
    val assignmentId: Long
}
