package com.utn.temaiken.core.entity.model

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table

@Table("zones_itineraries")
data class ZonesItinerary(
    @Id val id: Long,
    val zoneId: Long,
    val itineraryId: Long
)
