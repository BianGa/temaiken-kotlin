package com.utn.temaiken.core.entity.ecosystem

import java.time.LocalDateTime

data class AssignedSpeciesData(
    val spanishName: String,
    val scientificName: String,
    val habitat: String,
    val zone: String,
    val startingDate: LocalDateTime
)
