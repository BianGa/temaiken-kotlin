package com.utn.temaiken.core.entity.model

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table

@Table("users")
data class User(
    @Id val id: Long,
    val username: String,
    val password: String,
    val roleId: Long,
    val isActive: Boolean
)
