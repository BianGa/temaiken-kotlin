package com.utn.temaiken.core.entity.model

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table

@Table("species")
data class Species(
    @Id val id: Long,
    val spanishName: String,
    val scientificName: String,
    val description: String,
    val habitatId: Long,
    val zoneId: Long,
    val isActive: Boolean
)
