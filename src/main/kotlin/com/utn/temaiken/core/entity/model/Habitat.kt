package com.utn.temaiken.core.entity.model

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table

@Table("habitats")
data class Habitat(
    @Id val id: Long,
    val name: String,
    val weatherTypeId: String,
    val vegetationTypeId: String,
    val continentId: String,
    val isActive: Boolean
)
