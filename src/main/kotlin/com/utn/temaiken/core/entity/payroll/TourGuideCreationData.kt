package com.utn.temaiken.core.entity.payroll

import com.utn.temaiken.core.constants.Roles
import java.lang.Boolean.TRUE
import java.time.LocalDateTime

data class TourGuideCreationData(
    override val username: String,
    override val password: String,
    override val name: String,
    override val lastname: String,
    override val address: String,
    override val phone: String,
    override val hiringDate: LocalDateTime = LocalDateTime.now(),
    override val role: Roles = Roles.TOUR_GUIDE,
    override val isActive: Boolean = TRUE
) : PayrollCreationData
