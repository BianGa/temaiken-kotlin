package com.utn.temaiken.core.entity.ecosystem

data class WeatherTypeData(
    val id: String,
    val name: String,
)
