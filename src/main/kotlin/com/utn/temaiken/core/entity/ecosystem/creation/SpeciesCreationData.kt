package com.utn.temaiken.core.entity.ecosystem.creation

import com.utn.temaiken.core.entity.ecosystem.HabitatData
import com.utn.temaiken.core.entity.ecosystem.ZoneData
import java.lang.Boolean.TRUE

data class SpeciesCreationData(
    val spanishName: String,
    val scientificName: String,
    val description: String,
    val habitat: HabitatData,
    val zone: ZoneData,
    val isActive: Boolean = TRUE
)
