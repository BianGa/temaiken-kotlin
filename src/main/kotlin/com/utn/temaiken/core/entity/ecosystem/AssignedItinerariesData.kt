package com.utn.temaiken.core.entity.ecosystem

import java.math.BigDecimal
import java.time.LocalDateTime

data class AssignedItinerariesData(
    val code: String,
    val duration: Long,
    val longitude: BigDecimal,
    val maxVisitorsAmount: Int,
    val startsAt: LocalDateTime,
    val endsAt: LocalDateTime,
    val speciesToSee: List<SpeciesData>,
    val zonesToSee: List<ZoneData>
)
