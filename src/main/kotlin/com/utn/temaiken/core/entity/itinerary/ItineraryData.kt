package com.utn.temaiken.core.entity.itinerary

import java.time.LocalDateTime

data class ItineraryData(
    val code: String,
    val startsAt: LocalDateTime,
    val endsAt: LocalDateTime,
    val itineraryTimetableId: Long
)
