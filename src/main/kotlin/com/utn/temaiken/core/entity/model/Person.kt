package com.utn.temaiken.core.entity.model

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table

@Table("persons")
data class Person(
    @Id val id: Long,
    val userId: Long,
    val name: String,
    val lastname: String,
    val address: String,
    val phone: String,
    val isActive: Boolean
)
