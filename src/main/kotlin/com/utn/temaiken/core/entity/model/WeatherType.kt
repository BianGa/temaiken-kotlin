package com.utn.temaiken.core.entity.model

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table

@Table("weather_type")
data class WeatherType(
    @Id val id: String,
    val name: String,
)
