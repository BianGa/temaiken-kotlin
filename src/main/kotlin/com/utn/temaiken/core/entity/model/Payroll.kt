package com.utn.temaiken.core.entity.model

import java.time.LocalDateTime
import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table

@Table("payrolls")
data class Payroll(
    @Id val id: Long,
    val personId: Long,
    val roleId: Long,
    val hiringDate: LocalDateTime,
    val isActive: Boolean
)
