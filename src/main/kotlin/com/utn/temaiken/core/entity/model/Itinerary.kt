package com.utn.temaiken.core.entity.model

import java.math.BigDecimal
import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table

@Table("itineraries")
data class Itinerary(
    @Id val id: Long,
    val code: String,
    val duration: Long,
    val longitude: BigDecimal,
    val maxVisitorsAmount: Int,
)
