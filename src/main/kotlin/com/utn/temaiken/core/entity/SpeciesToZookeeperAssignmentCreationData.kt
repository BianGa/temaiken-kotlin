package com.utn.temaiken.core.entity

import java.time.LocalDateTime

data class SpeciesToZookeeperAssignmentCreationData(
    override val payrollId: Long, // zookeeperId
    override val assignmentId: Long, // speciesId
    val startingDate: LocalDateTime? = LocalDateTime.now()
) : AssigningCreationData
