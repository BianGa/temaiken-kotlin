package com.utn.temaiken.core.entity.model

import java.math.BigDecimal
import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table

@Table("zones")
data class Zone(
    @Id val id: Long,
    val name: String,
    val extension: BigDecimal,
    val isActive: Boolean
)
