package com.utn.temaiken.core.entity.ecosystem

data class VegetationTypeData(
    val id: String,
    val name: String,
)
