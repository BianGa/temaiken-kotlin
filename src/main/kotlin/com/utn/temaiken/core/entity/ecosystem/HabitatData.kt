package com.utn.temaiken.core.entity.ecosystem

data class HabitatData(
    val id: Long,
    val name: String,
)
