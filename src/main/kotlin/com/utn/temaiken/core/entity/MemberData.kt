package com.utn.temaiken.core.entity

import com.utn.temaiken.core.constants.Roles
import java.time.LocalDateTime

data class MemberData(
    val userId: Long,
    val username: String,
    val name: String,
    val lastname: String,
    val role: Roles,
    val payrollId: Long,
    val address: String,
    val phone: String,
    val hiringDate: LocalDateTime
)
