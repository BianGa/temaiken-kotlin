package com.utn.temaiken.core.entity.itinerary

import java.math.BigDecimal
import java.time.LocalDateTime

data class ItineraryCreationData(
    val code: String,
    val duration: Long,
    val longitude: BigDecimal,
    val maxVisitorsAmount: Int,
    val startsAt: LocalDateTime,
    val endsAt: LocalDateTime,
    val zones: List<Long>,
    val species: List<Long>
)
