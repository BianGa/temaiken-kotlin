package com.utn.temaiken.core.entity.model

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table

@Table("vegetation_type")
data class VegetationType(
    @Id val id: String,
    val name: String,
)
