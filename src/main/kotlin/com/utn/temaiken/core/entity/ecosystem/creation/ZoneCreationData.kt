package com.utn.temaiken.core.entity.ecosystem.creation

import java.lang.Boolean.TRUE
import java.math.BigDecimal

data class ZoneCreationData(
    val name: String,
    val extension: BigDecimal,
    val isActive: Boolean = TRUE
)
