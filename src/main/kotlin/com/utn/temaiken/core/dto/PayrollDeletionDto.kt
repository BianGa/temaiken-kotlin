package com.utn.temaiken.core.dto

data class PayrollDeletionDto(
    val username: String?,
    val name: String?,
    val lastname: String?
)
