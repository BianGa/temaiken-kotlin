package com.utn.temaiken.core.dto

data class LoggedUserDto(
    val name: String?,
    val lastname: String?,
    val role: String?
)
