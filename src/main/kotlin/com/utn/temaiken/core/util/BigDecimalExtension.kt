package com.utn.temaiken.core.util

import java.math.BigDecimal
import java.math.RoundingMode

fun BigDecimal.twoDecimalScale(): BigDecimal = this.setScale(2, RoundingMode.HALF_UP)
