package com.utn.temaiken

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication(
    scanBasePackages = [
        "com.utn.temaiken.integration.repository",
        "com.utn.temaiken.controller", "com.utn.temaiken.integration.services"
    ]
)
class TemaikenApplication

fun main(args: Array<String>) {

    runApplication<TemaikenApplication>(*args)
}
