package com.utn.temaiken.controller

import com.utn.temaiken.core.dto.LoggedUserDto
import com.utn.temaiken.core.entity.ecosystem.creation.ZoneCreationData
import com.utn.temaiken.integration.services.ZoneService
import java.math.BigDecimal
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.*

@Controller
@RequestMapping("/zone")
class ZoneController(
    private val zoneService: ZoneService
) {

    @GetMapping
    fun zone(
        model: Model,
        @ModelAttribute("loggedUser") loggedUser: LoggedUserDto
    ): String {
        model.addAttribute("loggedUser", loggedUser)
        return "ecosystem/creation/create-zone"
    }

    @PostMapping
    fun zone(
        model: Model,
        @ModelAttribute("loggedUser") loggedUser: LoggedUserDto,
        @RequestParam("zoneName") zoneName: String,
        @RequestParam("zoneExtension") zoneExtension: Double,
    ): String {
        model.addAttribute("loggedUser", loggedUser)
        zoneService.createZone(
            ZoneCreationData(
                name = zoneName,
                extension = BigDecimal.valueOf(zoneExtension)
            )
        ).let { result ->
            when (result) {
                0L -> {
                    model.addAttribute(
                        "errorMessage",
                        "La ZONA $zoneName no pudo ser creado"
                    )
                    model.addAttribute("type", "ZONA")
                    return "ecosystem/unable-to-create-ecosystem"
                }
                else -> {
                    model.addAttribute(
                        "createdMessage",
                        "La ZONA $zoneName ha sido creada con éxito"
                    )
                    model.addAttribute("type", "ZONA")
                    return "ecosystem/ecosystem-piece-created-successfully"
                }
            }
        }
    }

    @GetMapping("/delete")
    fun deleteZone(
        model: Model,
        @ModelAttribute("loggedUser") loggedUser: LoggedUserDto
    ): String {
        model.addAttribute("loggedUser", loggedUser)
        val activeZones = zoneService.retrieveAllActiveZones()
        if (activeZones.isEmpty()) return "ecosystem/no-zones-available"
        model.addAttribute("activeZones", activeZones)
        return "ecosystem/deletion/delete-zone"
    }

    @PostMapping("/delete")
    fun deleteZone(
        model: Model,
        @ModelAttribute("loggedUser") loggedUser: LoggedUserDto,
        @RequestParam(value = "zones", required = true) zones: List<String>
    ): String {
        model.addAttribute("loggedUser", loggedUser)
        val affectedZones = mutableListOf<String>()
        zones.map { zone ->
            zoneService.deleteZoneByName(zone).let {
                if (it == 1) affectedZones.add(zone)
            }
        }
        model.addAttribute("affectedZones", affectedZones)
        return "ecosystem/zones-deleted-successfully"
    }
}
