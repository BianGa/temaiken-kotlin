package com.utn.temaiken.controller

import com.utn.temaiken.core.dto.LoggedUserDto
import com.utn.temaiken.core.dto.PayrollDeletionDto
import com.utn.temaiken.core.entity.PayrollDeletionData
import com.utn.temaiken.integration.services.payroll.MemberRetrieverService
import com.utn.temaiken.integration.services.payroll.PayrollDeletionService
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.*

@Controller
@RequestMapping("/payroll")
class DeletePayrollController(
    val memberRetrieverService: MemberRetrieverService,
    val payrollDeletionService: PayrollDeletionService
) {

    @GetMapping("/deactivation")
    fun payroll(
        model: Model,
        @ModelAttribute("loggedUser") loggedUser: LoggedUserDto
    ): String {
        model.addAttribute("loggedUser", loggedUser)
        return "payroll/deactivation/delete-payroll"
    }

    @GetMapping("/deactivation/zookeeper")
    fun createZookeeper(
        model: Model,
        @ModelAttribute("loggedUser") loggedUser: LoggedUserDto
    ): String {
        model.addAttribute("loggedUser", loggedUser)
        return "payroll/deactivation/delete-zookeeper"
    }

    @GetMapping("/deactivation/tour-guide")
    fun createTourGuide(
        model: Model,
        @ModelAttribute("loggedUser") loggedUser: LoggedUserDto
    ): String {
        model.addAttribute("loggedUser", loggedUser)
        return "payroll/deactivation/delete-tour-guide"
    }

    @PostMapping("/deactivation/zookeeper")
    fun deleteZookeeper(
        model: Model,
        @ModelAttribute("loggedUser") loggedUser: LoggedUserDto,
        @RequestParam("disableName") disableName: String,
        @RequestParam("disableLastname") disableLastname: String,
        @RequestParam("disableUsername") disableUsername: String
    ): String {
        model.addAttribute("loggedUser", loggedUser)
        val deletionData = PayrollDeletionDto(
            username = disableName,
            name = disableLastname,
            lastname = disableUsername
        )
        val isUserDeactivated = deactivateUser(model, loggedUser, deletionData)
        return if (isUserDeactivated) "payroll/user-successfully-deleted"
        else "error"
    }

    @PostMapping("/deactivation/tour-guide")
    fun deleteTourGuide(
        model: Model,
        @ModelAttribute("loggedUser") loggedUser: LoggedUserDto,
        @RequestParam("disableName") disableName: String,
        @RequestParam("disableLastname") disableLastname: String,
        @RequestParam("disableUsername") disableUsername: String
    ): String {
        val deletionData = PayrollDeletionDto(
            username = disableName,
            name = disableLastname,
            lastname = disableUsername
        )
        val isUserDeactivated = deactivateUser(model, loggedUser, deletionData)
        return if (isUserDeactivated) "payroll/user-successfully-deleted"
        else "error" // TODO implement user not found html
    }

    private fun deactivateUser(
        model: Model,
        loggedUser: LoggedUserDto,
        deletionData: PayrollDeletionDto
    ): Boolean {

        // TODO v2 -> search admin names from database
        if (deletionData.name!!.uppercase() == "ZEUS")
            throw Exception("Admin users can not be deactivated")

        var deactivationSuccessful = false

        model.addAttribute("loggedUser", loggedUser)
        try {
            val memberData = memberRetrieverService.retrieveMemberByNameAndLastname(
                name = deletionData.name,
                lastname = deletionData.lastname!!
            )
            payrollDeletionService.deletePayroll(
                PayrollDeletionData(
                    userId = memberData.userId,
                    username = memberData.username,
                    name = memberData.name,
                    lastname = memberData.lastname
                )
            )
            model.addAttribute("userRole", memberData.role.description)
            model.addAttribute("memberName", "${memberData.name} ${memberData.lastname}")
            deactivationSuccessful = true
        } catch (e: Exception) {
            if (e is EmptyResultDataAccessException) deactivationSuccessful = false
        }
        return deactivationSuccessful
    }
}
