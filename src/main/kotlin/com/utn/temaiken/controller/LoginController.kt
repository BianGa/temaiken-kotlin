package com.utn.temaiken.controller

import com.utn.temaiken.core.constants.Roles
import com.utn.temaiken.core.constants.Roles.*
import com.utn.temaiken.core.dto.LoggedUserDto
import com.utn.temaiken.core.entity.LoggedUserData
import com.utn.temaiken.integration.services.UserLoginService
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam

@Controller
class LoginController(
    val userLoginService: UserLoginService
) {

    @PostMapping("/login")
    fun home(
        model: Model,
        @RequestParam(name = "username", required = true) username: String,
        @RequestParam(name = "password", required = true) password: String
    ): String {
        val loggedUser = try {
            userLoginService.retrieveActiveUser(username, password)
        } catch (e: Exception) {
            LoggedUserData(
                username = "",
                password = "",
                role = ERROR,
                name = "",
                lastname = "",
                address = "",
                phone = ""
            )
        }
        model.addAttribute("loggedUser", loggedUser)
        when (loggedUser.role) { // fix this uncertainty
            ADMIN -> return "home/admin"
            ZOOKEEPER -> return "home/zookeeper"
            TOUR_GUIDE -> return "home/tour-guide"
            VISITOR -> return "home/visitor"
            else -> return "error"
        }
        // TODO resolver el camino feliz de todos los tipos de usuario y luego armar el alta de usuarios cuando no se encuentra
        // TODO resolver user-not-found redirection better
    }

    @GetMapping("/home")
    fun home(
        model: Model,
        @ModelAttribute loggedUser: LoggedUserDto
    ): String {
        model.addAttribute("loggedUser", loggedUser)
        when (Roles.getByName(loggedUser.role!!)) {
            ADMIN -> return "home/admin"
            ZOOKEEPER -> return "home/zookeeper"
            TOUR_GUIDE -> return "home/tour-guide"
            VISITOR -> return "home/visitor"
            else -> return "error"
        }
    }

    @GetMapping("/logout")
    fun logout(): String {
        return "redirect:/"
    }
}
