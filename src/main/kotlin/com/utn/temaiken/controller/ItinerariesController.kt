package com.utn.temaiken.controller

import com.utn.temaiken.core.dto.LoggedUserDto
import com.utn.temaiken.core.entity.itinerary.ItineraryCreationData
import com.utn.temaiken.integration.services.ItineraryService
import com.utn.temaiken.integration.services.SpeciesService
import com.utn.temaiken.integration.services.ZoneService
import java.math.BigDecimal
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.*

@Controller
@RequestMapping("/itineraries")
class ItinerariesController(
    private val itineraryService: ItineraryService,
    private val zoneService: ZoneService,
    private val speciesService: SpeciesService,
) {

    @GetMapping
    fun createItinerary(
        model: Model,
        @ModelAttribute("loggedUser") loggedUser: LoggedUserDto
    ): String {
        model.addAttribute("loggedUser", loggedUser)

        val allActiveSpecies = speciesService.retrieveAllActiveSpecies()
        val allActiveZones = zoneService.retrieveAllActiveZones()

        if (allActiveSpecies.isEmpty()) return "ecosystem/no-species-available"
        if (allActiveZones.isEmpty()) return "ecosystem/no-zones-available"

        model.addAttribute("allActiveSpecies", allActiveSpecies)
        model.addAttribute("allActiveZones", allActiveZones)

        return "itineraries/create-itinerary"
    }

    @PostMapping
    fun createItinerary(
        model: Model,
        @ModelAttribute("loggedUser") loggedUser: LoggedUserDto,
        @RequestParam(value = "code", required = true) code: String,
        @RequestParam(value = "duration", required = true) duration: Long,
        @RequestParam(value = "longitude", required = true) longitude: BigDecimal,
        @RequestParam(value = "maxVisitorsAmount", required = true) maxVisitorsAmount: Int,
        @RequestParam(value = "startsAt", required = true) startsAt: String,
        @RequestParam(value = "species", required = true) species: List<Long>,
        @RequestParam(value = "zones", required = true) zones: List<Long>,
    ): String {
        // TODO V2: pedir startsAt y endsAt, validar que las fecha y hora de ends no sea menor que starts y viceversa y calcular la duration
        val parsedStartsAt = LocalDateTime.parse(startsAt.replace('T', ' '), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"))
        val itineraryCreationData = ItineraryCreationData(
            code = code,
            duration = duration,
            longitude = longitude,
            maxVisitorsAmount = maxVisitorsAmount,
            startsAt = parsedStartsAt,
            endsAt = calculateFromStartsAt(parsedStartsAt, duration),
            zones = zones,
            species = species
        )

        itineraryService.createItinerary(itineraryCreationData)
            .let { result ->
                when (result) {
                    0L -> {
                        model.addAttribute(
                            "errorMessage",
                            "El ITINERARIO con el código $code no pudo ser creado"
                        )
                        model.addAttribute("type", "ITINERARIO")
                        return "ecosystem/unable-to-create-ecosystem"
                    }
                    else -> {
                        model.addAttribute(
                            "createdMessage",
                            "El ITINERARIO con el código $code fue creado con éxito"
                        )
                        model.addAttribute("type", "ITINERARIO")
                        return "ecosystem/ecosystem-piece-created-successfully"
                    }
                }
            }
    }

    // TODO v2: select created itinerary and assign multiple timetables
    // execute itinerary forever on a given day of the week and assigned time
    // execute itinerary for X amount of weeks, or every X amount of days until a certain date

    private fun calculateFromStartsAt(startsAt: LocalDateTime, duration: Long): LocalDateTime =
        startsAt.plusMinutes(duration)
}
