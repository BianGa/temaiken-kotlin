package com.utn.temaiken.controller

import com.utn.temaiken.core.dto.LoggedUserDto
import com.utn.temaiken.core.entity.ecosystem.VegetationTypeData
import com.utn.temaiken.core.entity.ecosystem.WeatherTypeData
import com.utn.temaiken.core.entity.ecosystem.creation.HabitatCreationData
import com.utn.temaiken.core.entity.model.Continent
import com.utn.temaiken.integration.services.HabitatService
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*

@Controller
@RequestMapping("/habitat")
class HabitatController(
    private val habitatService: HabitatService
) {

    @GetMapping
    fun habitat(
        model: Model,
        @ModelAttribute("loggedUser") loggedUser: LoggedUserDto
    ): String {
        model.addAttribute("loggedUser", loggedUser)
        val everyWeather = habitatService.findWeatherTypes()
        val everyVegetation = habitatService.findVegetationTypes()
        val everyContinent = habitatService.findContinents()

        model.addAttribute("everyWeather", everyWeather)
        model.addAttribute("everyVegetation", everyVegetation)
        model.addAttribute("everyContinent", everyContinent)

        return "ecosystem/creation/create-habitat"
    }

    @PostMapping
    fun habitat(
        model: Model,
        @ModelAttribute("loggedUser") loggedUser: LoggedUserDto,
        @RequestParam(value = "habitatName", required = true) habitatName: String,
        @Validated selectedWeather: String,
        @Validated selectedVegetation: String,
        @Validated selectedContinent: String
    ): String {
        model.addAttribute("loggedUser", loggedUser)
        val habitatCreationData = HabitatCreationData(
            name = habitatName,
            weather = retrieveSelectedWeather(selectedWeather),
            vegetation = retrieveSelectedVegetation(selectedVegetation),
            continent = retrieveSelectedContinent(selectedContinent)
        )

        habitatService.createHabitat(habitatCreationData).let { result ->
            when (result) {
                0L -> {
                    model.addAttribute(
                        "errorMessage",
                        "El HÁBITAT $habitatName no pudo ser creado"
                    )
                    model.addAttribute("type", "HÁBITAT")
                    return "ecosystem/unable-to-create-ecosystem"
                }
                else -> {
                    model.addAttribute(
                        "createdMessage",
                        "El HÁBITAT $habitatName ha sido creado con éxito"
                    )
                    model.addAttribute("type", "HÁBITAT")
                    return "ecosystem/ecosystem-piece-created-successfully"
                }
            }
        }
    }

    @GetMapping("/delete")
    fun deleteHabitat(
        model: Model,
        @ModelAttribute("loggedUser") loggedUser: LoggedUserDto,
    ): String {
        model.addAttribute("loggedUser", loggedUser)
        val activeHabitats = habitatService.retrieveAllActiveHabitats()
        if (activeHabitats.isEmpty()) return "ecosystem/no-habitats-available"
        model.addAttribute("activeHabitats", activeHabitats)
        return "ecosystem/deletion/delete-habitat"
    }

    @PostMapping("/delete")
    fun deleteHabitat(
        model: Model,
        @ModelAttribute("loggedUser") loggedUser: LoggedUserDto,
        @RequestParam(value = "habitats", required = true) habitats: List<String>
    ): String {
        model.addAttribute("loggedUser", loggedUser)
        val affectedHabitats = mutableListOf<String>()
        habitats.map { habitat ->
            habitatService.deleteHabitatByName(habitat).let {
                if (it == 1) affectedHabitats.add(habitat)
            }
        }
        model.addAttribute("affectedHabitats", affectedHabitats)
        return "ecosystem/habitats-deleted-successfully"
    }

    private fun retrieveSelectedWeather(selectedWeather: String) =
        WeatherTypeData(
            id = selectedWeather.substringAfterLast("id=").substringBefore(','),
            name = selectedWeather.substringAfterLast("name=").substringBefore(')')
        )

    private fun retrieveSelectedVegetation(selectedVegetation: String) =
        VegetationTypeData(
            id = selectedVegetation.substringAfterLast("id=").substringBefore(','),
            name = selectedVegetation.substringAfterLast("name=").substringBefore(')')
        )

    private fun retrieveSelectedContinent(selectedContinent: String) =
        Continent(
            id = selectedContinent.substringAfterLast("id=").substringBefore(','),
            name = selectedContinent.substringAfterLast("name=").substringBefore(')')
        )
}
