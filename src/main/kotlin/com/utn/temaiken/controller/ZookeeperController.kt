package com.utn.temaiken.controller

import com.utn.temaiken.core.constants.Roles
import com.utn.temaiken.core.dto.LoggedUserDto
import com.utn.temaiken.core.entity.MemberData
import com.utn.temaiken.integration.services.AssignmentService
import com.utn.temaiken.integration.services.payroll.MemberRetrieverService
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@RequestMapping("/zookeeper")
class ZookeeperController(
    private val retrieveMember: MemberRetrieverService,
    private val assignmentService: AssignmentService
) {

    @GetMapping("/self")
    fun self(
        model: Model,
        @ModelAttribute("loggedUser") loggedUser: LoggedUserDto
    ): String {
        val memberInfo = getMember(loggedUser)
        model.addAttribute("member", memberInfo)
        model.addAttribute("memberRole", Roles.ZOOKEEPER.description)
        return "payroll/data/payroll-data"
    }

    @GetMapping("/species")
    fun assignedSpecies(
        model: Model,
        @ModelAttribute("loggedUser") loggedUser: LoggedUserDto
    ): String {
        val zookeeper = getMember(loggedUser)
        val assignedSpecies = assignmentService.retrieveAssignedSpecies(zookeeper.payrollId)

        if (assignedSpecies.isEmpty()) return "ecosystem/no-species-available"

        model.addAttribute("memberRole", Roles.ZOOKEEPER.description)
        model.addAttribute("assignedSpecies", assignedSpecies)

        return "payroll/data/assigned-species"
    }

    private fun getMember(loggedUser: LoggedUserDto): MemberData =
        retrieveMember.retrieveMemberByNameAndLastname(
            name = loggedUser.name!!,
            lastname = loggedUser.lastname!!
        )
}
