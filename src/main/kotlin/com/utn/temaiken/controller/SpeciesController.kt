package com.utn.temaiken.controller

import com.utn.temaiken.core.dto.LoggedUserDto
import com.utn.temaiken.core.entity.ecosystem.HabitatData
import com.utn.temaiken.core.entity.ecosystem.ZoneData
import com.utn.temaiken.core.entity.ecosystem.creation.SpeciesCreationData
import com.utn.temaiken.integration.services.HabitatService
import com.utn.temaiken.integration.services.SpeciesService
import com.utn.temaiken.integration.services.ZoneService
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*

@Controller
@RequestMapping("/species")
class SpeciesController(
    private val speciesService: SpeciesService,
    private val habitatService: HabitatService,
    private val zoneService: ZoneService
) {

    @GetMapping
    fun species(
        model: Model,
        @ModelAttribute("loggedUser") loggedUser: LoggedUserDto
    ): String {
        model.addAttribute("loggedUser", loggedUser)
        val allActiveHabitats = habitatService.retrieveAllActiveHabitats()
        val allActiveZones = zoneService.retrieveAllActiveZones()
        if (allActiveHabitats.isEmpty()) return "ecosystem/no-habitats-available"
        if (allActiveZones.isEmpty()) return "ecosystem/no-zones-available"
        model.addAttribute("allActiveHabitats", allActiveHabitats)
        model.addAttribute("allActiveZones", allActiveZones)
        return "ecosystem/creation/create-species"
    }

    @PostMapping
    fun species(
        model: Model,
        @ModelAttribute("loggedUser") loggedUser: LoggedUserDto,
        @RequestParam("spanishName") spanishName: String,
        @RequestParam("scientificName") scientificName: String,
        @RequestParam("description") description: String,
        @Validated selectedHabitat: String,
        @Validated selectedZone: String,
    ): String {
        model.addAttribute("loggedUser", loggedUser)
        speciesService.createSpecies(
            SpeciesCreationData(
                spanishName = spanishName,
                scientificName = scientificName,
                description = description,
                habitat = retrieveHabitatData(selectedHabitat),
                zone = retrieveZoneData(selectedZone),
            )
        ).let { result ->
            when (result) {
                0L -> {
                    model.addAttribute(
                        "errorMessage",
                        "La ESPECIE $spanishName no pudo ser creada"
                    )
                    model.addAttribute("type", "ESPECIE")
                    return "ecosystem/unable-to-create-ecosystem"
                }
                else -> {
                    model.addAttribute(
                        "createdMessage",
                        "La ESPECIE $spanishName ha sido creada con éxito"
                    )
                    model.addAttribute("type", "ESPECIE")
                    return "ecosystem/ecosystem-piece-created-successfully"
                }
            }
        }
    }

    @GetMapping("/delete")
    fun deleteSpecies(
        model: Model,
        @ModelAttribute("loggedUser") loggedUser: LoggedUserDto
    ): String {
        model.addAttribute("loggedUser", loggedUser)
        val activeSpecies = speciesService.retrieveAllActiveSpecies()
        if (activeSpecies.isEmpty()) return "ecosystem/no-species-available"
        model.addAttribute("activeSpecies", activeSpecies)
        return "ecosystem/deletion/delete-species"
    }

    @PostMapping("/delete")
    fun deleteSpecies(
        model: Model,
        @ModelAttribute("loggedUser") loggedUser: LoggedUserDto,
        @RequestParam(value = "selectedSpecies", required = true) selectedSpecies: List<String>
    ): String {
        model.addAttribute("loggedUser", loggedUser)
        val affectedSpecies = mutableListOf<String>()
        selectedSpecies.map { species ->
            speciesService.deleteSpeciesBySpanishName(species).let {
                if (it == 1) affectedSpecies.add(species)
            }
        }
        model.addAttribute("affectedSpecies", affectedSpecies)
        return "ecosystem/species-deleted-successfully"
    }

    private fun retrieveHabitatData(habitat: String) =
        HabitatData(
            id = habitat.substringAfter("id=").substringBefore(",").toLong(),
            name = habitat.substringAfter("name=").substringBefore(",")
        )

    private fun retrieveZoneData(zone: String) =
        ZoneData(
            id = zone.substringAfter("id=").substringBefore(",").toLong(),
            name = zone.substringAfter("name=").substringBefore(",")
        )
}
