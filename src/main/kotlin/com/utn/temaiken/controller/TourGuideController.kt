package com.utn.temaiken.controller

import com.utn.temaiken.core.constants.Roles
import com.utn.temaiken.core.dto.LoggedUserDto
import com.utn.temaiken.core.entity.MemberData
import com.utn.temaiken.integration.services.AssignmentService
import com.utn.temaiken.integration.services.payroll.MemberRetrieverService
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@RequestMapping("/tour-guide")
class TourGuideController(
    private val retrieveMember: MemberRetrieverService,
    private val assignmentService: AssignmentService
) {

    @GetMapping("/self")
    fun self(
        model: Model,
        @ModelAttribute("loggedUser") loggedUser: LoggedUserDto
    ): String {
        val memberInfo = getMember(loggedUser)
        model.addAttribute("member", memberInfo)
        model.addAttribute("memberRole", Roles.TOUR_GUIDE.description)
        return "payroll/data/payroll-data"
    }

    @GetMapping("/itineraries")
    fun assignedItineraries(
        model: Model,
        @ModelAttribute("loggedUser") loggedUser: LoggedUserDto
    ): String {
        val tourGuide = getMember(loggedUser)
        val assignedItineraries = assignmentService.retrieveAssignedItineraries(tourGuide.payrollId)

        if (assignedItineraries.isEmpty()) return "ecosystem/no-itineraries-available"

        model.addAttribute("memberRole", Roles.TOUR_GUIDE.description)
        model.addAttribute("assignedItineraries", assignedItineraries)

        return "payroll/data/assigned-itineraries"
    }

    private fun getMember(loggedUser: LoggedUserDto): MemberData =
        retrieveMember.retrieveMemberByNameAndLastname(
            name = loggedUser.name!!,
            lastname = loggedUser.lastname!!
        )
}
