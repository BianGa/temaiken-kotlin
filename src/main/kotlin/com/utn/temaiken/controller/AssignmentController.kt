package com.utn.temaiken.controller

import com.utn.temaiken.core.constants.Roles
import com.utn.temaiken.core.dto.LoggedUserDto
import com.utn.temaiken.core.entity.ItineraryToTourGuideAssignmentCreationData
import com.utn.temaiken.core.entity.SpeciesToZookeeperAssignmentCreationData
import com.utn.temaiken.integration.services.AssignmentService
import com.utn.temaiken.integration.services.ItineraryService
import com.utn.temaiken.integration.services.SpeciesService
import com.utn.temaiken.integration.services.payroll.MemberRetrieverService
import java.time.LocalDateTime
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@RequestMapping("/assignment")
class AssignmentController(
    private val memberRetrieverService: MemberRetrieverService,
    private val speciesService: SpeciesService,
    private val assignmentService: AssignmentService,
    private val itineraryService: ItineraryService,
) {

    @GetMapping("/zookeeper")
    fun speciesAssignmentToZookeeper(
        model: Model,
        @ModelAttribute("loggedUser") loggedUser: LoggedUserDto
    ): String {
        model.addAttribute("loggedUser", loggedUser)
        val allActiveSpecies = speciesService.retrieveAllActiveSpecies()
        val allActiveZookeepers = memberRetrieverService.retrieveAllActiveMembersByRole(Roles.ZOOKEEPER)

        if (allActiveSpecies.isEmpty()) return "ecosystem/no-species-available"
        if (allActiveZookeepers.isEmpty()) return "payroll/no-zookeepers-available"

        model.addAttribute("allActiveSpecies", allActiveSpecies)
        model.addAttribute("allActiveZookeepers", allActiveZookeepers)
        return "payroll/assignment/species-to-zookeeper"
    }

    @PostMapping("/zookeeper")
    fun speciesAssignmentToZookeeper(
        model: Model,
        @ModelAttribute("loggedUser") loggedUser: LoggedUserDto,
        @Validated selectedSpecies: String,
        @Validated selectedZookeeper: String
    ): String {
        model.addAttribute("loggedUser", loggedUser)

        assignmentService.assignSpeciesToZookeeper(
            SpeciesToZookeeperAssignmentCreationData(
                payrollId = retrievePayrollId(selectedZookeeper),
                assignmentId = retrieveSpeciesId(selectedSpecies)
            )
        ).let { result ->
            when (result) {
                0L -> {
                    model.addAttribute(
                        "errorMessage",
                        "La asignación no pudo ser completada" // TODO V2 mejorar el mensaje agregando datos
                    )
                    model.addAttribute("type", "ASIGNAR ESPECIE A CUIDADOR")
                    return "ecosystem/unable-to-create-ecosystem"
                }
                else -> {
                    model.addAttribute(
                        "createdMessage",
                        "La asignación ha sido realizada con éxito"
                    )
                    model.addAttribute("type", "ASIGNAR ESPECIE A CUIDADOR") // idem arriba
                    return "ecosystem/ecosystem-piece-created-successfully"
                }
            }
        }
    }

    @GetMapping("/tour-guide")
    fun itineraryAssignmentToTourGuide(
        model: Model,
        @ModelAttribute("loggedUser") loggedUser: LoggedUserDto
    ): String {
        model.addAttribute("loggedUser", loggedUser)

        val everyItineraryData = itineraryService.retrieveEveryActiveItinerary(LocalDateTime.now())
        val allActiveTourGuides = memberRetrieverService.retrieveAllActiveMembersByRole(Roles.TOUR_GUIDE)

        if (everyItineraryData.isEmpty()) return "ecosystem/no-itineraries-available"
        if (allActiveTourGuides.isEmpty()) return "payroll/no-tour-guides-available"

        model.addAttribute("everyItineraryData", everyItineraryData)
        model.addAttribute("allActiveTourGuides", allActiveTourGuides)

        return "payroll/assignment/itinerary-to-tour-guide"
    }

    @PostMapping("/tour-guide")
    fun itineraryAssignmentToTourGuide(
        model: Model,
        @ModelAttribute("loggedUser") loggedUser: LoggedUserDto,
        @Validated selectedItinerary: String,
        @Validated selectedTourGuide: String
    ): String {
        model.addAttribute("loggedUser", loggedUser)

        assignmentService.assignItineraryToTourGuide(
            ItineraryToTourGuideAssignmentCreationData(
                payrollId = retrievePayrollId(selectedTourGuide),
                assignmentId = retrieveItineraryTimetableId(selectedItinerary),
            )
        ).let { result ->
            when (result) {
                0L -> {
                    model.addAttribute(
                        "errorMessage",
                        "El itinerario no pudo ser asignado al guía."
                    )
                    model.addAttribute("type", "ITINERARIO")
                    return "ecosystem/unable-to-create-ecosystem"
                }
                else -> {
                    model.addAttribute(
                        "createdMessage",
                        "El itinerario fue asignado con éxito."
                    )
                    model.addAttribute("type", "ITINERARIO")
                    return "ecosystem/ecosystem-piece-created-successfully"
                }
            }
        }
    }

    private fun retrievePayrollId(selectedMember: String): Long =
        selectedMember.substringAfter("payrollId=").substringBefore(",").toLong()

    private fun retrieveSpeciesId(selectedSpecies: String): Long =
        selectedSpecies.substringAfter("id=").substringBefore(",").toLong()

    private fun retrieveItineraryTimetableId(selectedItinerary: String): Long =
        selectedItinerary.substringAfter("itineraryTimetableId=").substringBefore(")").toLong()
}
