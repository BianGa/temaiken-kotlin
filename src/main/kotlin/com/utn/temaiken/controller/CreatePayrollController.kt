package com.utn.temaiken.controller

import com.utn.temaiken.core.constants.Roles.TOUR_GUIDE
import com.utn.temaiken.core.constants.Roles.ZOOKEEPER
import com.utn.temaiken.core.dto.LoggedUserDto
import com.utn.temaiken.core.entity.payroll.TourGuideCreationData
import com.utn.temaiken.core.entity.payroll.ZookeeperCreationData
import com.utn.temaiken.integration.services.UserLoginService
import com.utn.temaiken.integration.services.payroll.PayrollCreationService
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.*

@Controller
@RequestMapping("/payroll")
class CreatePayrollController(
    val userLoginService: UserLoginService,
    val payrollCreationService: PayrollCreationService
) {

    @GetMapping
    fun payroll(
        model: Model,
        @ModelAttribute("loggedUser") loggedUser: LoggedUserDto
    ): String {
        model.addAttribute("loggedUser", loggedUser)
        return "payroll/creation/create-payroll"
    }

    @GetMapping("/zookeeper")
    fun createZookeeper(
        model: Model,
        @ModelAttribute("loggedUser") loggedUser: LoggedUserDto
    ): String {
        model.addAttribute("loggedUser", loggedUser)
        return "payroll/creation/create-zookeeper"
    }

    @GetMapping("/tour-guide")
    fun createTourGuide(
        model: Model,
        @ModelAttribute("loggedUser") loggedUser: LoggedUserDto
    ): String {
        model.addAttribute("loggedUser", loggedUser)
        return "payroll/creation/create-tour-guide"
    }

    @PostMapping("/zookeeper")
    fun createZookeeper(
        model: Model,
        @ModelAttribute("loggedUser") loggedUser: LoggedUserDto,
        @RequestParam("username") username: String,
        @RequestParam("password") password: String,
        @RequestParam("newName") name: String,
        @RequestParam("newLastname") lastname: String,
        @RequestParam("address") address: String,
        @RequestParam("phone") phone: String,
    ): String {
        model.addAttribute("loggedUser", loggedUser)
        if (isUserNew(username, password)) {
            payrollCreationService.createPayroll(
                ZookeeperCreationData(
                    username = username,
                    password = password,
                    name = name,
                    lastname = lastname,
                    address = address,
                    phone = phone
                )
            )
            model.addAttribute("userRole", ZOOKEEPER.description)
            model.addAttribute("createdUserName", username)
            return "payroll/user-successfully-created"
        } else {
            model.addAttribute("createdUserName", username)
            return "payroll/user-already-created"
        }
    }

    @PostMapping("/tour-guide")
    fun createTourGuide(
        model: Model,
        @ModelAttribute("loggedUser") loggedUser: LoggedUserDto,
        @RequestParam("username") username: String,
        @RequestParam("password") password: String,
        @RequestParam("name") name: String,
        @RequestParam("lastname") lastname: String,
        @RequestParam("address") address: String,
        @RequestParam("phone") phone: String,
    ): String {
        model.addAttribute("loggedUser", loggedUser)
        if (isUserNew(username, password)) {
            payrollCreationService.createPayroll(
                TourGuideCreationData(
                    username = username,
                    password = password,
                    name = name,
                    lastname = lastname,
                    address = address,
                    phone = phone
                )
            )
            model.addAttribute("userRole", TOUR_GUIDE.description)
            model.addAttribute("createdUserName", username)
            return "payroll/user-successfully-created"
        } else {
            model.addAttribute("createdUserName", username)
            return "payroll/user-already-created"
        }
    }

    private fun isUserNew(username: String, password: String): Boolean {
        var userNew = false
        try {
            userLoginService.retrieveExistingUserByUsernameAndPassword(username, password)
        } catch (e: Exception) {
            userNew = e is EmptyResultDataAccessException
        }
        return userNew
    }
}
