package com.utn.temaiken.integration.repository

import com.utn.temaiken.core.entity.model.WeatherType
import org.springframework.data.jdbc.repository.query.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface WeatherTypeRepository : CrudRepository<WeatherType, String> {

    @Query("select * from weather_type")
    fun findWeatherTypes(): List<WeatherType>
}
