package com.utn.temaiken.integration.repository

import com.utn.temaiken.core.entity.model.Zone
import org.springframework.data.jdbc.repository.query.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface ZoneRepository : CrudRepository<Zone, Long> {

    @Query("select * from zones")
    fun findZones(): List<Zone>

    fun findByName(name: String): Zone
    fun findByIsActive(isActive: Boolean? = true): List<Zone>
}
