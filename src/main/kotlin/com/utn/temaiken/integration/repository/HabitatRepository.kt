package com.utn.temaiken.integration.repository

import com.utn.temaiken.core.entity.model.Habitat
import org.springframework.data.jdbc.repository.query.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface HabitatRepository : CrudRepository<Habitat, Long> {

    @Query("select * from habitats")
    fun findZones(): List<Habitat>

    fun findByName(name: String): Habitat
    fun findByIsActive(isActive: Boolean? = true): List<Habitat>
}
