package com.utn.temaiken.integration.repository

import com.utn.temaiken.core.entity.model.VegetationType
import org.springframework.data.jdbc.repository.query.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface VegetationTypeRepository : CrudRepository<VegetationType, String> {

    @Query("select * from vegetation_type")
    fun findVegetationTypes(): List<VegetationType>
}
