package com.utn.temaiken.integration.repository

import com.utn.temaiken.core.entity.model.ZonesItinerary
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface ZonesItinerariesRepository : CrudRepository<ZonesItinerary, Long> {

    fun findByItineraryId(itineraryId: Long): List<ZonesItinerary>
}
