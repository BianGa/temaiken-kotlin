package com.utn.temaiken.integration.repository

import com.utn.temaiken.core.entity.model.Continent
import org.springframework.data.jdbc.repository.query.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface ContinentRepository : CrudRepository<Continent, String> {

    @Query("select * from continents")
    fun findContinents(): List<Continent>
}
