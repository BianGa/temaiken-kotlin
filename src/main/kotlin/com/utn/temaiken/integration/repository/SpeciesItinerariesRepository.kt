package com.utn.temaiken.integration.repository

import com.utn.temaiken.core.entity.model.SpeciesItinerary
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface SpeciesItinerariesRepository : CrudRepository<SpeciesItinerary, Long> {

    fun findByItineraryId(itineraryId: Long): List<SpeciesItinerary>
}
