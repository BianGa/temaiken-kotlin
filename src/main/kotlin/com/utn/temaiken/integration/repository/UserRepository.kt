package com.utn.temaiken.integration.repository

import com.utn.temaiken.core.entity.model.User
import org.springframework.data.jdbc.repository.query.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface UserRepository : CrudRepository<User, Long> {

    @Query("select * from users")
    fun findUsers(): List<User>

    fun findByUsernameAndPasswordAndIsActive(
        username: String,
        password: String,
        active: Boolean? = true
    ): User
    fun findByUsernameAndPassword(username: String, password: String): User
}
