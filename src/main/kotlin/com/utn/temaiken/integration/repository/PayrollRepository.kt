package com.utn.temaiken.integration.repository

import com.utn.temaiken.core.entity.model.Payroll
import org.springframework.data.jdbc.repository.query.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface PayrollRepository : CrudRepository<Payroll, Long> {

    @Query("select * from payrolls")
    fun findCompletePayroll(): List<Payroll>

    fun findByPersonId(personId: Long): Payroll
}
