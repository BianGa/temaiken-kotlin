package com.utn.temaiken.integration.repository

import com.utn.temaiken.core.entity.model.Species
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface SpeciesRepository : CrudRepository<Species, Long> {

    fun findBySpanishName(spanishName: String): Species

    fun findByIsActive(isActive: Boolean? = true): List<Species>
}
