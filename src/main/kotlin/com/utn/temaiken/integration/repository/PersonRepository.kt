package com.utn.temaiken.integration.repository

import com.utn.temaiken.core.entity.model.Person
import org.springframework.data.jdbc.repository.query.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface PersonRepository : CrudRepository<Person, Long> {

    @Query("select * from persons")
    fun findPersons(): List<Person>

    fun findByUserId(userId: Long): Person
    fun findByNameAndLastnameAndIsActive(name: String, lastname: String, isActive: Boolean? = true): Person
}
