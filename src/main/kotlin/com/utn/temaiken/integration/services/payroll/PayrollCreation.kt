package com.utn.temaiken.integration.services.payroll

import com.utn.temaiken.core.entity.payroll.PayrollCreationData

interface PayrollCreation {

    fun <T : PayrollCreationData> createPayroll(payrollCreationData: T)
}
