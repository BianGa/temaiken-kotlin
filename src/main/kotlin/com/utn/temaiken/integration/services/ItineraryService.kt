package com.utn.temaiken.integration.services

import com.utn.temaiken.configurations.database.DatabaseClient
import com.utn.temaiken.core.entity.itinerary.ItineraryCreationData
import com.utn.temaiken.core.entity.itinerary.ItineraryData
import java.time.LocalDateTime
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.stereotype.Service

@Service
class ItineraryService {

    // TODO V2: con el mismo código se puede agregar otro timetable pero no debería tener otra duración, longitud ni máximo
    private val database = DatabaseClient()

    // V1: creation only | V2: validate if itinerary by code, duration, longitude and maxVisitorsAmount is not already created
    // V2: if composite key of itinerary is found, return it and edit it or do some other thing with it
    // TODO: transactional creation, roll back creation if something goes wrong
    fun createItinerary(itineraryCreationData: ItineraryCreationData): Long {
        var createdItinerary = 0L
        var createdTimetable = 0L
        val createdSpeciesItineraries = mutableListOf<Long>()
        val createdZoneItineraries = mutableListOf<Long>()
        try {
            database.executeQuery(
                """
                    INSERT INTO itineraries (code, duration, longitude, max_visitors_amount)
                    VALUES ($1, $2, $3, $4)
                    RETURNING id;
                """.trimIndent(),
                params = listOf(
                    "'${itineraryCreationData.code}'", // $1
                    itineraryCreationData.duration, // $2
                    itineraryCreationData.longitude, // $3
                    itineraryCreationData.maxVisitorsAmount // $4
                )
            ).let { resultSet ->
                while (resultSet.next()) {
                    createdItinerary = resultSet.getLong("id")
                }
            }.also {
                itineraryCreationData.species.map { speciesId ->
                    database.executeQuery(
                        """
                            INSERT INTO species_itineraries (itinerary_id, species_id)
                            VALUES ($1, $2)
                            RETURNING id;
                        """.trimIndent(),
                        params = listOf(
                            createdItinerary, // $1
                            speciesId // $2
                        )
                    ).let { resultSet ->
                        while (resultSet.next()) {
                            createdSpeciesItineraries.add(resultSet.getLong("id"))
                        }
                    }
                }
                itineraryCreationData.zones.map { zoneId ->
                    database.executeQuery(
                        """
                            INSERT INTO zones_itineraries (itinerary_id, zone_id)
                            VALUES ($1, $2)
                            RETURNING id;
                        """.trimIndent(),
                        params = listOf(
                            createdItinerary, // $1
                            zoneId // $2
                        )
                    ).let { resultSet ->
                        while (resultSet.next()) {
                            createdZoneItineraries.add(resultSet.getLong("id"))
                        }
                    }
                }
            }.also {
                database.executeQuery(
                    """
                            INSERT INTO itineraries_timetable (itinerary_id, starts_at, ends_at)
                            VALUES ($1, $2, $3)
                            RETURNING id;
                    """.trimIndent(),
                    params = listOf(
                        createdItinerary, // $1
                        "'${itineraryCreationData.startsAt}'", // $2
                        "'${itineraryCreationData.endsAt}'" // $3
                    )
                ).let { resultSet ->
                    while (resultSet.next()) {
                        createdTimetable = resultSet.getLong("id")
                    }
                }
            }
        } catch (e: Exception) {
            if (e is EmptyResultDataAccessException) createdTimetable = 0L
        }
        return createdTimetable
    }

    fun retrieveEveryActiveItinerary(from: LocalDateTime): List<ItineraryData> {
        val activeItineraries = mutableListOf<ItineraryData>()

        try {
            database.executeQuery(
                """
                    SELECT 
                        i.code,
                        it.id,
                        it.starts_at,
                        it.ends_at
                    FROM itineraries_timetable AS it 
                    JOIN itineraries AS i ON it.itinerary_id = i.id
                    WHERE it.starts_at >= $1
                """.trimIndent(),
                params = listOf(
                    "'${LocalDateTime.now()}'"
                )
            ).let { resultSet ->
                while (resultSet.next()) {
                    activeItineraries.add(
                        ItineraryData(
                            code = resultSet.getString("code"),
                            startsAt = resultSet.getTimestamp("starts_at").toLocalDateTime(),
                            endsAt = resultSet.getTimestamp("ends_at").toLocalDateTime(),
                            itineraryTimetableId = resultSet.getLong("id")
                        )
                    )
                }
            }
        } catch (e: Exception) {
            if (e is EmptyResultDataAccessException) activeItineraries
        }

        return activeItineraries
    }
}
