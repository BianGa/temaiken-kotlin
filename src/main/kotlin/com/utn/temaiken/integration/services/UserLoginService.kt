package com.utn.temaiken.integration.services

import com.utn.temaiken.core.constants.Roles
import com.utn.temaiken.core.entity.LoggedUserData
import com.utn.temaiken.integration.repository.PersonRepository
import com.utn.temaiken.integration.repository.UserRepository
import org.springframework.stereotype.Service

@Service
class UserLoginService constructor(
    private val userRepository: UserRepository,
    private val personRepository: PersonRepository
) {

    fun retrieveActiveUser(username: String, password: String): LoggedUserData =
        userRepository.findByUsernameAndPasswordAndIsActive(username, password).let { user ->
            personRepository.findByUserId(user.id).let { person ->
                LoggedUserData(
                    username = user.username,
                    password = user.password,
                    role = Roles.getById(user.roleId),
                    name = person.name,
                    lastname = person.lastname,
                    address = person.address,
                    phone = person.phone
                )
            }
        }

    fun retrieveExistingUserByUsernameAndPassword(username: String, password: String): LoggedUserData =
        userRepository.findByUsernameAndPassword(username, password).let { user ->
            personRepository.findByUserId(user.id).let { person ->
                LoggedUserData(
                    username = user.username,
                    password = user.password,
                    role = Roles.getById(user.roleId),
                    name = person.name,
                    lastname = person.lastname,
                    address = person.address,
                    phone = person.phone
                )
            }
        }
}
