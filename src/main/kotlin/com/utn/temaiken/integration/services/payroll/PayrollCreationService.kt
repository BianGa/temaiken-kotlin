package com.utn.temaiken.integration.services.payroll

import com.utn.temaiken.configurations.database.DatabaseClient
import com.utn.temaiken.core.entity.model.User
import com.utn.temaiken.core.entity.payroll.PayrollCreationData
import com.utn.temaiken.integration.repository.PersonRepository
import com.utn.temaiken.integration.repository.UserRepository
import org.springframework.stereotype.Service

@Service
class PayrollCreationService constructor(
    private val userRepository: UserRepository,
    private val personRepository: PersonRepository
) : PayrollCreation {

    private val database = DatabaseClient()
    // To be done in next feature
    fun showUsersByRole(): List<User> = userRepository.findUsers()

    override fun <T : PayrollCreationData> createPayroll(payrollCreationData: T) {
        database.executeQuery(
            """
                INSERT INTO users (username, password, role_id, is_active)
                VALUES ($1, $2, $3, $4)
                RETURNING id;
            """.trimIndent(),
            listOf(
                "'${payrollCreationData.username}'", // $1
                "'${payrollCreationData.password}'", // $2
                payrollCreationData.role.id, // $3
                payrollCreationData.isActive // $4
            )
        ).let {
            userRepository.findByUsernameAndPasswordAndIsActive(
                payrollCreationData.username, payrollCreationData.password
            ).also { user ->
                database.executeQuery(
                    """
                        INSERT INTO persons (user_id, name, lastname, address, phone, is_active)
                        VALUES ($1, $2, $3, $4, $5, $6)
                        RETURNING id;
                    """.trimIndent(),
                    listOf(
                        user.id, // $1
                        "'${payrollCreationData.name}'", // $2
                        "'${payrollCreationData.lastname}'", // $3
                        "'${payrollCreationData.address}'", // $4
                        "'${payrollCreationData.phone}'", // $5
                        "${payrollCreationData.isActive}" // $6
                    )
                ).let {
                    personRepository.findByUserId(user.id).also { person ->
                        database.executeQuery(
                            """
                                INSERT INTO payrolls (person_id, role_id, hiring_date, is_active)
                                VALUES ($1, $2, $3, $4)
                                RETURNING id;
                            """.trimIndent(),
                            listOf(
                                person.id, // $1
                                payrollCreationData.role.id, // $2
                                "'${payrollCreationData.hiringDate}'", // $3
                                payrollCreationData.isActive.toString() // $4
                            )
                        )
                    }
                }
            }
        }
    }
}
