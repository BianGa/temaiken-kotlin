package com.utn.temaiken.integration.services

import com.utn.temaiken.configurations.database.DatabaseClient
import com.utn.temaiken.core.entity.ItineraryToTourGuideAssignmentCreationData
import com.utn.temaiken.core.entity.SpeciesToZookeeperAssignmentCreationData
import com.utn.temaiken.core.entity.ecosystem.AssignedItinerariesData
import com.utn.temaiken.core.entity.ecosystem.AssignedSpeciesData
import com.utn.temaiken.core.entity.ecosystem.SpeciesData
import com.utn.temaiken.core.entity.ecosystem.ZoneData
import com.utn.temaiken.integration.repository.SpeciesItinerariesRepository
import com.utn.temaiken.integration.repository.SpeciesRepository
import com.utn.temaiken.integration.repository.ZoneRepository
import com.utn.temaiken.integration.repository.ZonesItinerariesRepository
import java.time.LocalDate
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.stereotype.Service

@Service
class AssignmentService(
    private val speciesItinerariesRepository: SpeciesItinerariesRepository,
    private val speciesRepository: SpeciesRepository,
    private val zonesItinerariesRepository: ZonesItinerariesRepository,
    private val zoneRepository: ZoneRepository
) {

    private val database = DatabaseClient()

    fun assignSpeciesToZookeeper(creationData: SpeciesToZookeeperAssignmentCreationData): Long {
        var successfulAssignment = 0L
        try {
            database.executeQuery(
                """
                    INSERT INTO species_zookeeper (zookeeper_id, species_id, starting_date)
                    VALUES ($1, $2, $3)
                    RETURNING id;
                """.trimIndent(),
                params = listOf(
                    creationData.payrollId, // $1
                    creationData.assignmentId, // $2
                    "'${creationData.startingDate}'"
                )
            ).let { resultSet ->
                while (resultSet.next()) {
                    successfulAssignment = resultSet.getLong("id")
                }
            }
        } catch (e: Exception) {
            if (e is EmptyResultDataAccessException) successfulAssignment = 0L
        }
        return successfulAssignment
    }

    fun retrieveAssignedSpecies(zookeeperId: Long): List<AssignedSpeciesData> {
        val speciesAssigned = mutableListOf<AssignedSpeciesData>()
        try {
            database.executeQuery(
                """
                    SELECT
                        s.spanish_name,
                        s.scientific_name, 
                        h.name AS habitat,
                        z.name AS zone,
                        sz.starting_date
                    FROM species AS s
                    JOIN habitats AS h ON s.habitat_id = h.id
                    JOIN zones AS z ON s.zone_id = z.id
                    JOIN species_zookeeper AS sz ON s.id = sz.species_id
                    WHERE sz.zookeeper_id = $1
                """.trimIndent(),
                params = listOf(
                    zookeeperId // $1
                )
            ).let { resultSet ->
                while (resultSet.next()) {
                    speciesAssigned.add(
                        AssignedSpeciesData(
                            spanishName = resultSet.getString("spanish_name"),
                            scientificName = resultSet.getString("scientific_name"),
                            habitat = resultSet.getString("habitat"),
                            zone = resultSet.getString("zone"),
                            startingDate = resultSet.getTimestamp("starting_date").toLocalDateTime()
                        )
                    )
                }
            }
        } catch (e: Exception) {
            if (e is EmptyResultDataAccessException) speciesAssigned
        }
        return speciesAssigned
    }

    fun assignItineraryToTourGuide(creationData: ItineraryToTourGuideAssignmentCreationData): Long {
        var successfulAssignment = 0L
        try {
            database.executeQuery(
                """
                    INSERT INTO tour_guide_itineraries (tour_guide_id, itinerary_timetable_id)
                    VALUES ($1, $2)
                    RETURNING id;
                """.trimIndent(),
                params = listOf(
                    creationData.payrollId, // $1
                    creationData.assignmentId, // $2
                )
            ).let { resultSet ->
                while (resultSet.next()) {
                    successfulAssignment = resultSet.getLong("id")
                }
            }
        } catch (e: Exception) {
            if (e is EmptyResultDataAccessException) successfulAssignment = 0L
        }
        return successfulAssignment
    }

    fun retrieveAssignedItineraries(tourGuideId: Long): List<AssignedItinerariesData> {
        val itineraries = mutableListOf<AssignedItinerariesData>()
        val itineraryIds = mutableListOf<Long>()
        val today = LocalDate.now().atStartOfDay()

        try {
            database.executeQuery(
                """
                    SELECT itinerary_id 
                    FROM itineraries_timetable AS it
                    JOIN tour_guide_itineraries AS tgi ON it.id = tgi.itinerary_timetable_id
                    WHERE tgi.tour_guide_id = $1 AND it.starts_at >= $2;
                """.trimIndent(),
                params = listOf(
                    tourGuideId, // $1
                    "'$today'" // $2
                )
            ).let { resultSet ->
                while (resultSet.next()) {
                    itineraryIds.add(resultSet.getLong("itinerary_id"))
                }
            }.runCatching {
                itineraryIds.map { itineraryId ->
                    val speciesToSee = speciesItinerariesRepository.findByItineraryId(itineraryId).map {
                        speciesRepository.findById(it.speciesId).let { optionalSpecies ->
                            val species = if (optionalSpecies.isPresent) optionalSpecies.get() else throw Exception("Especie no encontrada")
                            SpeciesData(spanishName = species.spanishName)
                        }
                    }

                    val zonesToSee = zonesItinerariesRepository.findByItineraryId(itineraryId).map {
                        zoneRepository.findById(it.zoneId).let { optionalZone ->
                            val zone = if (optionalZone.isPresent) optionalZone.get() else throw Exception("Zona no encontrada")
                            ZoneData(id = zone.id, name = zone.name)
                        }
                    }

                    database.executeQuery(
                        """
                            SELECT 
                                i.code,
                                i.duration,
                                i.longitude,
                                i.max_visitors_amount,
                                it.starts_at,
                                it.ends_at
                            FROM itineraries AS i
                            JOIN itineraries_timetable AS it ON i.id = it.itinerary_id
                            WHERE i.id = $1;
                        """.trimIndent(),
                        params = listOf(itineraryId)
                    ).let { resultSet ->
                        while (resultSet.next()) {
                            itineraries.add(
                                AssignedItinerariesData(
                                    code = resultSet.getString("code"),
                                    duration = resultSet.getLong("duration"),
                                    longitude = resultSet.getBigDecimal("longitude"),
                                    maxVisitorsAmount = resultSet.getInt("max_visitors_amount"),
                                    startsAt = resultSet.getTimestamp("starts_at").toLocalDateTime(),
                                    endsAt = resultSet.getTimestamp("ends_at").toLocalDateTime(),
                                    speciesToSee = speciesToSee,
                                    zonesToSee = zonesToSee
                                )
                            )
                        }
                    }
                }
            }
        } catch (e: Exception) {
            if (e is EmptyResultDataAccessException) itineraries
        }

        return itineraries
    }
}
