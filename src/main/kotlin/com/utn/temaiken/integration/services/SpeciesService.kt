package com.utn.temaiken.integration.services

import com.utn.temaiken.configurations.database.DatabaseClient
import com.utn.temaiken.core.entity.ecosystem.creation.SpeciesCreationData
import com.utn.temaiken.core.entity.model.Species
import com.utn.temaiken.integration.repository.SpeciesRepository
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.stereotype.Service

@Service
class SpeciesService constructor(
    private val speciesRepository: SpeciesRepository
) {

    private val database = DatabaseClient()

    fun createSpecies(speciesCreationData: SpeciesCreationData): Long {
        var createdId = 0L
        try {
            speciesRepository.findBySpanishName(speciesCreationData.spanishName)
        } catch (e: Exception) {
            if (e is EmptyResultDataAccessException) {
                database.executeQuery(
                    """
                        INSERT INTO species (spanish_name, scientific_name, description, habitat_id, zone_id, is_active)
                        VALUES ($1, $2, $3, $4, $5, $6)
                        RETURNING id;
                    """.trimIndent(),
                    listOf(
                        "'${speciesCreationData.spanishName}'", // $1
                        "'${speciesCreationData.scientificName}'", // $2
                        "'${speciesCreationData.description}'", // $3
                        speciesCreationData.habitat.id, // $4
                        speciesCreationData.zone.id, // $5
                        speciesCreationData.isActive // $6
                    )
                ).let { resultSet ->
                    while (resultSet.next()) {
                        createdId = resultSet.getLong("id")
                    }
                }
            }
        }
        return createdId
    }

    fun retrieveAllActiveSpecies(): List<Species> = speciesRepository.findByIsActive()

    fun deleteSpeciesBySpanishName(speciesSpanishName: String): Int =
        database.executeUpdate(
            """
                UPDATE species 
                SET is_active = false 
                WHERE spanish_name = $1;
            """.trimIndent(),
            listOf(
                "'$speciesSpanishName'" // $1
            )
        )
}
