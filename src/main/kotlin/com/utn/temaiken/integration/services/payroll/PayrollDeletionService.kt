package com.utn.temaiken.integration.services.payroll

import com.utn.temaiken.core.entity.PayrollDeletionData
import com.utn.temaiken.core.entity.model.User
import com.utn.temaiken.integration.repository.UserRepository
import org.springframework.stereotype.Service

@Service
class PayrollDeletionService constructor(
    private val userRepository: UserRepository
) {

    /** deletion means inactivation of user */
    fun deletePayroll(payrollDeletionData: PayrollDeletionData) {
        userRepository.findById(payrollDeletionData.userId).let { optionalUser ->
            val user = if (optionalUser.isPresent) optionalUser.get() else throw Exception("")
            userRepository.save(
                User(
                    id = user.id,
                    username = user.username,
                    password = user.password,
                    roleId = user.roleId,
                    isActive = false
                )
            )
        }
    }
}
