package com.utn.temaiken.integration.services.payroll

import com.utn.temaiken.configurations.database.DatabaseClient
import com.utn.temaiken.core.constants.Roles
import com.utn.temaiken.core.entity.MemberData
import com.utn.temaiken.integration.repository.PayrollRepository
import com.utn.temaiken.integration.repository.PersonRepository
import com.utn.temaiken.integration.repository.UserRepository
import org.springframework.stereotype.Service

@Service
class MemberRetrieverService constructor(
    private val personRepository: PersonRepository,
    private val userRepository: UserRepository,
    private val payrollRepository: PayrollRepository
) {

    private val database = DatabaseClient()

    fun retrieveMemberByNameAndLastname(name: String, lastname: String): MemberData =
        personRepository.findByNameAndLastnameAndIsActive(name, lastname).let { person ->
            userRepository.findById(person.userId).let { optionalUser ->
                // TODO implement custom exception
                val user = if (optionalUser.isPresent) optionalUser.get() else throw Exception("user not present")
                payrollRepository.findByPersonId(person.id).let { payroll ->
                    MemberData(
                        userId = user.id,
                        username = user.username,
                        name = person.name,
                        lastname = person.lastname,
                        role = Roles.getById(user.roleId),
                        payrollId = payroll.id,
                        address = person.address,
                        phone = person.phone,
                        hiringDate = payroll.hiringDate
                    )
                }
            }
        }

    fun retrieveAllActiveMembersByRole(role: Roles): List<MemberData> {
        val activeMembers = mutableListOf<MemberData>()
        database.executeQuery(
            """
                SELECT
                    u.id AS user_id,
                    u.username AS username,
                    p.name AS name,
                    p.lastname AS lastname,
                    u.role_id AS role_id,
                    py.id AS payroll_id,
                    p.address AS address,
                    p.phone AS phone,
                    py.hiring_date AS hiring_date
                FROM users AS u
                JOIN persons AS p
                    ON u.id = p.user_id
                JOIN payrolls AS py
                    ON p.id = py.person_id
                WHERE u.role_id = $1
                    AND u.is_active = true;
            """.trimIndent(),
            listOf(
                role.id // $1
            )
        ).let { resultSet ->
            while (resultSet.next()) {
                activeMembers.add(
                    MemberData(
                        userId = resultSet.getLong("user_id"),
                        username = resultSet.getString("username"),
                        name = resultSet.getString("name"),
                        lastname = resultSet.getString("lastname"),
                        role = Roles.getById(resultSet.getLong("role_id")),
                        payrollId = resultSet.getLong("payroll_id"),
                        address = resultSet.getString("address"),
                        phone = resultSet.getString("phone"),
                        hiringDate = resultSet.getTimestamp("hiring_date").toLocalDateTime()
                    )
                )
            }
        }
        return activeMembers
    }
}
