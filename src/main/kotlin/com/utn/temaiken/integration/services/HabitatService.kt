package com.utn.temaiken.integration.services

import com.utn.temaiken.configurations.database.DatabaseClient
import com.utn.temaiken.core.entity.ecosystem.creation.HabitatCreationData
import com.utn.temaiken.core.entity.model.Habitat
import com.utn.temaiken.integration.repository.ContinentRepository
import com.utn.temaiken.integration.repository.HabitatRepository
import com.utn.temaiken.integration.repository.VegetationTypeRepository
import com.utn.temaiken.integration.repository.WeatherTypeRepository
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.stereotype.Service

@Service
class HabitatService constructor(
    private val habitatRepository: HabitatRepository,
    private val weatherTypeRepository: WeatherTypeRepository,
    private val vegetationTypeRepository: VegetationTypeRepository,
    private val continentRepository: ContinentRepository
) {

    private val database = DatabaseClient()

    fun createHabitat(habitatCreationData: HabitatCreationData): Long {
        var createdId = 0L
        try {
            habitatRepository.findByName(habitatCreationData.name)
        } catch (e: Exception) {
            if (e is EmptyResultDataAccessException) {
                database.executeQuery(
                    """
                        INSERT INTO habitats (name, vegetation_type_id, weather_type_id, continent_id, is_active)
                        VALUES ($1, $2, $3, $4, $5)
                        RETURNING id;
                    """.trimIndent(),
                    listOf(
                        "'${habitatCreationData.name}'", // $1
                        "'${habitatCreationData.vegetation.id}'", // $2
                        "'${habitatCreationData.weather.id}'", // $3
                        "'${habitatCreationData.continent.id}'", // $4
                        habitatCreationData.isActive // $5
                    )
                ).let { resultSet ->
                    while (resultSet.next()) {
                        createdId = resultSet.getLong("id")
                    }
                }
            }
        }
        return createdId
    }

    fun findWeatherTypes() = weatherTypeRepository.findWeatherTypes()
    fun findVegetationTypes() = vegetationTypeRepository.findVegetationTypes()
    fun findContinents() = continentRepository.findContinents()

    // TODO v2 change query to retrieve spanish readable information of climate, vegetation and continent
    fun retrieveAllActiveHabitats(): List<Habitat> = habitatRepository.findByIsActive()

    fun deleteHabitatByName(habitatName: String): Int =
        database.executeUpdate(
            """
                UPDATE habitats
                SET is_active = false
                WHERE name = $1;
            """.trimIndent(),
            listOf(
                "'$habitatName'" // $1
            )
        )
}
