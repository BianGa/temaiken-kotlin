package com.utn.temaiken.integration.services

import com.utn.temaiken.configurations.database.DatabaseClient
import com.utn.temaiken.core.entity.ecosystem.creation.ZoneCreationData
import com.utn.temaiken.core.entity.model.Zone
import com.utn.temaiken.integration.repository.ZoneRepository
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.stereotype.Service

@Service
class ZoneService constructor(
    private val zoneRepository: ZoneRepository
) {

    private val database = DatabaseClient()

    fun createZone(zoneCreationData: ZoneCreationData): Long {
        var createdId = 0L
        try {
            zoneRepository.findByName(zoneCreationData.name)
        } catch (e: Exception) {
            if (e is EmptyResultDataAccessException) {
                database.executeQuery(
                    """
                        INSERT INTO zones (name, extension, is_active)
                        VALUES ($1, $2, $3)
                        RETURNING id;
                    """.trimIndent(),
                    listOf(
                        "'${zoneCreationData.name}'", // $1
                        zoneCreationData.extension, // $2
                        zoneCreationData.isActive // $3
                    )
                ).let { resultSet ->
                    while (resultSet.next()) {
                        createdId = resultSet.getLong("id")
                    }
                }
            }
        }
        return createdId
    }

    fun retrieveAllActiveZones(): List<Zone> = zoneRepository.findByIsActive()

    fun deleteZoneByName(zoneName: String): Int =
        database.executeUpdate(
            """
                UPDATE zones 
                SET is_active = false 
                WHERE name = $1;
            """.trimIndent(),
            listOf(
                "'$zoneName'" // $1
            )
        )
}
