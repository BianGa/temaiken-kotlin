package com.utn.temaiken.configurations.database

import javax.sql.DataSource
import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class DataSourceConfig {

    @Bean
    fun getDataSource(): DataSource {
        val dataSourceBuilder = DataSourceBuilder.create()
        dataSourceBuilder.driverClassName("org.postgresql.Driver")
        dataSourceBuilder.url("jdbc:postgresql://localhost:5432/postgres")
        dataSourceBuilder.username("root")
        dataSourceBuilder.password("root")
        return dataSourceBuilder.build()
    }

    @Bean
    fun getTestDataSource(): DataSource {
        val dataSourceBuilder = DataSourceBuilder.create()
        dataSourceBuilder.driverClassName("org.postgresql.Driver")
        dataSourceBuilder.url("jdbc:postgresql://localhost:5432/temaiken")
        dataSourceBuilder.username("root")
        dataSourceBuilder.password("root")
        return dataSourceBuilder.build()
    }
}
