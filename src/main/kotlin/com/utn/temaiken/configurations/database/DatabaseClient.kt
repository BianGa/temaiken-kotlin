package com.utn.temaiken.configurations.database

import java.sql.Connection
import java.sql.ResultSet

open class DatabaseClient {

    companion object {
        private var dataSource = DataSourceConfig()
        private var connection: Connection? = null

        @JvmName("getManualConnection")
        fun getConnection(): Connection {
            if (connection == null) {
                connection = dataSource.getDataSource().connection
            }
            return connection!!
        }
    }

    fun executeQuery(
        query: String,
        params: List<Any?> = listOf()
    ): ResultSet {
        var preparedQuery = query
        if (params.isNotEmpty()) {
            params.mapIndexed { idx, value ->
                preparedQuery = preparedQuery.replace("$${idx + 1}", value.toString())
                // TODO fix string values recognition
            }
        } else preparedQuery = query
        return getConnection().createStatement().executeQuery(preparedQuery)
    }

    fun executeUpdate(
        query: String,
        params: List<Any?> = listOf()
    ): Int {
        var preparedQuery = query
        if (params.isNotEmpty()) {
            params.mapIndexed { idx, value ->
                preparedQuery = preparedQuery.replace("$${idx + 1}", value.toString())
                // TODO fix string values recognition
            }
        } else preparedQuery = query
        return getConnection().createStatement().executeUpdate(preparedQuery)
    }
}
